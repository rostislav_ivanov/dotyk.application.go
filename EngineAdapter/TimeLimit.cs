﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineAdapter
{
    public enum DecreaseTimeResult { Ok, Ended, Buyomi, BuyomiNotStarted };

    public class TimeLimit
    {
        public int Id { get; set; }

        public int MainTime { get; set; }
        public int CurrentBuyomi { get; set; }
        public int CurrentStones { get; set; }

        public int InitialBuyomi { get; set; }
        public int InitialStones { get; set; }

        public TimeLimit(int mainTime, int buyomi, int stones)
        {
            this.MainTime = mainTime;
            CurrentBuyomi = buyomi;
            CurrentStones = 0;

            InitialBuyomi = buyomi;
            InitialStones = stones;

            previousTurnMainTime = MainTime;
            previousTurnCurrentBuyomi = CurrentBuyomi;
        }

        public TimeLimit Copy()
        {
            return new TimeLimit(MainTime, InitialBuyomi, InitialStones);
        }

        public DecreaseTimeResult DecreaseTime()
        {
            if (MainTime > 0)
            {
                MainTime--;
                if (MainTime <= 0 && CurrentBuyomi > 0)
                    return DecreaseTimeResult.Buyomi;
                else if (InitialBuyomi > 0)
                    return DecreaseTimeResult.BuyomiNotStarted;
                else
                    return DecreaseTimeResult.Ok;
            }
            else if(CurrentBuyomi > 0)
            {
                CurrentBuyomi--;
                return DecreaseTimeResult.Ok;
            }
            else
            {
                return DecreaseTimeResult.Ended;
            }
        }

        private int previousTurnMainTime = -1;
        private int previousTurnCurrentBuyomi = -1;
        public void Move()
        {
            previousTurnMainTime = MainTime;
            previousTurnCurrentBuyomi = CurrentBuyomi;

            if (MainTime > 0)
                return;

            if (CurrentStones < InitialStones - 1)
                CurrentStones++;
            else
            {
                CurrentBuyomi = InitialBuyomi;
                CurrentStones = 0;
            }
        }

        public void Undo(bool returnTime)
        {
            if(returnTime)
            {
                MainTime =      previousTurnMainTime;
                CurrentBuyomi = previousTurnCurrentBuyomi;
            }
            else if(CurrentStones > 0)
                CurrentStones--;
        }

        public string InitialTimeToSting()
        {
            return MainTime.ToString(CultureInfo.InvariantCulture) + ' ' + InitialBuyomi.ToString(CultureInfo.InvariantCulture) + ' ' + InitialStones.ToString(CultureInfo.InvariantCulture);
        }

        public string LeftTimeToSting()
        {
            return (MainTime + CurrentBuyomi).ToString(CultureInfo.InvariantCulture) + ' ' + CurrentStones.ToString(CultureInfo.InvariantCulture);
        }

        public string TimeFormat()
        {
            if (InitialStones > 0 && MainTime <= 0)
                return new TimeSpan(0, 0, CurrentBuyomi).ToString(@"mm\:ss");
            else
                return new TimeSpan(0, 0, MainTime).ToString(@"mm\:ss");
        }

        public string StonesFormat()
        {
            return CurrentStones.ToString() + '/' + InitialStones.ToString();
        }
    }
}
