﻿using System;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Dotyk.Application.Go.Services;
using Microsoft.Practices.Unity;
using Windows.UI.Xaml.Media;
using Dotyk.Application.Go.Views;

namespace Dotyk.Application.Go
{
    sealed partial class App : DotykApplication
    {
        public static string Args;

        public App() : base(true)
        {
            InitializeComponent();
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        {
            base.OnLaunched(e);

            await Initialized;

            OnFrameContentReady(); //if this is called after 2 second your app will be closed


            if (RootFrame.Content == null)
                RootFrame.Navigate(typeof(InitPage), e.Arguments);
            
            Window.Current.CoreWindow.Activate();
            
            //Args = e.Arguments;
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            base.OnWindowCreated(args);
        }
    }
}