﻿using EngineAdapter;
using EngineAdapter.Engine;
using Prism.Windows.Mvvm;

// ReSharper disable ExplicitCallerInfoArgument

namespace Dotyk.Application.Go.ViewModels
{
    public class PlayerViewModel : ViewModelBase
    {
        GoPlayer player;

        public PlayerViewModel(GoPlayer p, GoColor color)
        {
            _color = color;
            _name = p.Name;
            _playerType = p.PlayerType;
            _level = p.Level;
            _komi = p.Komi;
            player = p;
        }

        #region MoveCount
        private int _moveCount;
        public int MoveCount
        {
            get => _moveCount; set { _moveCount = value; RaisePropertyChanged(); }
        }
        #endregion MoveCount

        private string _name;
        public string Name
        {
            get => _name; set => SetProperty(ref _name, value);
        }

        private PlayerType _playerType;
        public PlayerType PlayerType
        {
            get => _playerType; set => SetProperty(ref _playerType, value);
        }

        private int _level;
        public int Level
        {
            get => _level; set => SetProperty(ref _level, value);
        }

        private GoColor _color;
        public GoColor Color
        {
            get => _color; set => SetProperty(ref _color, value);
        }

        private decimal _komi;
        public decimal Komi
        {
            get => _komi; set
            {
                if (SetProperty(ref _komi, value))
                    RaisePropertyChanged(nameof(ScoreChinese));
            }
        }

        private int _area;
        public int Area
        {
            get => _area; set
            {
                if (SetProperty(ref _area, value))
                    RaisePropertyChanged(nameof(ScoreChinese));
            }
        }

        private int _alive;
        public int Alive
        {
            get => _alive; set
            {
                if (SetProperty(ref _alive, value))
                    RaisePropertyChanged(nameof(ScoreChinese));
            }
        }

        #region Captured
        private int _captured;
        public int Captured
        {
            get => _captured;
            set
            {
                if (SetProperty(ref _captured, value, nameof(Captured)))
                {
                    RaisePropertyChanged(nameof(Prisoners));
                    RaisePropertyChanged(nameof(ScoreChinese));
                }
            }
        }
        #endregion Captured

        #region Dead
        private int _dead;
        public int Dead
        {
            get => _dead;
            set
            {
                if (SetProperty(ref _dead, value, nameof(Dead)))
                {
                    RaisePropertyChanged(nameof(ScoreChinese));
                }
            }
        }
        #endregion Dead

        public decimal ScoreChinese => _komi + _area;
        public decimal ScoreJapanese => _komi + _area - _alive + Prisoners;
        public int Prisoners => _captured + _dead;

        public TimeLimit LeftTime
        {
            get
            {
                return player.LeftTime;
            }
            set
            {
                player.LeftTime = value;
            }
        }
    }
}