﻿using Common;
using Common.DotykUserControls;
using Dotyk.Application.Go.Model;
using Dotyk.Application.Go.Strings;
using Dotyk.Application.Go.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class MenuControl : UserControl
    {
        Line[] horizontalLines;
        Line[] verticalLines;
        bool isNeedToRotate;
        bool isFirst;
        MenuButton humanButton;

        public MenuControl(int rowsCount, int columnsCount, LocalizationItem[] texts, bool isNeetToTurnAround = false, bool isFlipped = false, bool isRowsCloseRowEachOther = false, bool isFirst = false, bool isOneButtonBig = false)
        {
            this.InitializeComponent();

            isNeedToRotate = isNeetToTurnAround;
            this.isFirst = isFirst;
            humanButton = null;

            LayoutRoot.ColumnDefinitions.Clear();
            LayoutRoot.RowDefinitions.Clear();

            if (!isRowsCloseRowEachOther)
            {
                for (int h = 0; h < rowsCount; h++)
                    LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            else
            {
                LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0 / 6, GridUnitType.Star) });

                LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0 / 3, GridUnitType.Star) });
                LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0 / 3, GridUnitType.Star) });

                LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0 / 6, GridUnitType.Star) });
            }


            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.75, GridUnitType.Star) });

            for (int v = 0; v < columnsCount; v++)
                LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.75, GridUnitType.Star) });

            horizontalLines = new Line[rowsCount];
            for (int h = 0; h < rowsCount; h++)
            {
                horizontalLines[h] = new Line()
                {
                    Stroke = new SolidColorBrush(Colors.Black),
                    StrokeThickness = Table.IsFullScreen ? 2 : 1,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    X1 = -5,
                    X2 = -5
                };
                horizontalLines[h].SetValue(Grid.RowProperty, !isRowsCloseRowEachOther ? h : h + 1);
                horizontalLines[h].SetValue(Grid.ColumnSpanProperty, columnsCount + 2);
                horizontalLines[h].Loaded += MenuControl_Loaded;
                LayoutRoot.Children.Add(horizontalLines[h]);
            }

            verticalLines = new Line[columnsCount];
            for (int v = 0; v < columnsCount; v++)
            {
                verticalLines[v] = new Line()
                {
                    Stroke = new SolidColorBrush(Colors.Black),
                    StrokeThickness = Table.IsFullScreen ? 2 : 1,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Y1 = -5,
                    Y2 = -5
                };
                verticalLines[v].SetValue(Grid.ColumnProperty, v + 1);
                verticalLines[v].SetValue(Grid.RowSpanProperty, !isRowsCloseRowEachOther ? rowsCount : rowsCount + 2);
                verticalLines[v].Loaded += MenuControl_Loaded;
                LayoutRoot.Children.Add(verticalLines[v]);
            }

            MenuButton menuButton;
            int valuesIndex = 0;
            for (int h = 0; h < rowsCount; h++)
            {
                for (int v = 0; v < columnsCount; v++)
                {
                    menuButton = new MenuButton();
                    menuButton.Pressed += ButtonControl_Pressed;
                    menuButton.IsWhite = (h + v) % 2 == 0;
                    menuButton.SetValue(Grid.RowProperty, !isRowsCloseRowEachOther ? h : h + 1);
                    menuButton.SetValue(Grid.ColumnProperty, v + 1);
                    menuButton.Value = valuesIndex;
                    menuButton.HorizontalLine = horizontalLines[h];
                    menuButton.VerticalLine = verticalLines[v];
                    menuButton.Text = texts[valuesIndex];
                    menuButton.IsNeetToTurnAround = Table.IsFullScreen && isNeetToTurnAround;
                    menuButton.IsFlipped = h==0 ? isFlipped : false;
                    //menuButton.Diameter = Table.IsFullScreen ? 600 : 300;
                    //menuButton.FontSize = Table.IsFullScreen ? 60 : 40;
                    menuButton.VerticalAlignment = VerticalAlignment.Stretch;
                    menuButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                    if (isFlipped)
                        menuButton.MarginPercantage = 0.3;
                    else if(columnsCount == 3 && rowsCount == 2)
                        menuButton.MarginPercantage = 0.1;
                    else if(isOneButtonBig && v == 1)
                        menuButton.MarginPercantage = 0.3;



                    LayoutRoot.Children.Add(menuButton);

                    if (isFirst && valuesIndex == 1)
                        humanButton = menuButton;

                    valuesIndex++;
                }
            }

            SizeChanged += MenuControl_SizeChanged;
        }

        private async void MenuControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (App.Current.RootFrame.ActualHeight == 0 || Double.IsNaN(App.Current.RootFrame.ActualHeight) || !linesAnimationCompleted)
                return;

            if (verticalLines != null)
            {
                foreach (var line in verticalLines)
                {
                    line.Y2 = App.Current.RootFrame.ActualHeight;
                }
            }

            if (horizontalLines != null)
            {
                foreach (var line in horizontalLines)
                {
                    line.X2 = App.Current.RootFrame.ActualWidth;
                }
            }

            foreach (var child in LayoutRoot.Children)
            {
                if (child is MenuButton)
                {
                    MenuButton menuButton = child as MenuButton;
                    //menuButton.Diameter = isFullScreen ? 600 : 300;
                    //menuButton.FontSize = isFullScreen ? 80 : 40;
                    menuButton.IsNeetToTurnAround = Table.IsFullScreen && isNeedToRotate;
                }
                else if (child is Line)
                {
                    ((Line)child).StrokeThickness = BoardLineThickness();
                }
            }
        }

        TimeSpan timeSpanAnimations = TimeSpan.FromSeconds(0.25);

        private void MenuControl_Loaded(object sender, RoutedEventArgs e)
        {
            bool isHorizontal = ((Line)sender).X1 == -5; 

            Storyboard storyboard = new Storyboard();
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.From = -5;
            doubleAnimation.EasingFunction = new SineEase() { EasingMode = EasingMode.EaseIn };
            doubleAnimation.To = isHorizontal ? App.Current.RootFrame.ActualWidth : App.Current.RootFrame.ActualHeight;
            doubleAnimation.Duration = new Duration(timeSpanAnimations);
            doubleAnimation.EnableDependentAnimation = true;
            storyboard.Children.Add(doubleAnimation);
            Storyboard.SetTargetProperty(doubleAnimation, isHorizontal ? "X2" : "Y2");
            Storyboard.SetTarget(doubleAnimation, (Line)sender);
            storyboard.Begin();
            storyboard.Completed += Storyboard_Completed;
            //if (isFirst)
            //    App.Current.ShellClient.OnApplicationSlotSizeChanged += ShellClient_OnApplicationSlotSizeChanged;

            this.Unloaded += MenuControl_Unloaded;
        }

        bool linesAnimationCompleted = false;
        private void Storyboard_Completed(object sender, object e)
        {
            linesAnimationCompleted = true;
        }

        private void MenuControl_Unloaded(object sender, RoutedEventArgs e)
        {
            foreach (var child in LayoutRoot.Children)
            {
                if (child is MenuButton)
                {
                    MenuButton menuButton = child as MenuButton;
                    menuButton.Pressed -= ButtonControl_Pressed;
                }
            }
        }

        public event EventHandler<int> ParameterIsSelected;


        private async void ButtonControl_Pressed(object sender, EventArgs e)
        {
            MenuButton button = sender as MenuButton; 
            /*if (isFirst)
            {
                if(button.Value == 1  && App.Current.ShellClient.ApplicationSlotSize != Shell.Utils.Session.ApplicationSlotSize.Fullscreen)
                {
                    button.Text = "Drag\r\nthe arrow";
                    await Task.Delay(3000);
                    button.Text = "Human";
                    button.HorizontalLine.Stroke = new SolidColorBrush(Colors.White);
                    button.VerticalLine.Stroke = new SolidColorBrush(Colors.White);
                    return;
                }
            }*/

            foreach(var child in LayoutRoot.Children)
            {
                if(child is MenuButton && button.Value != (child as MenuButton).Value)
                {
                    Storyboard storyboard = ((MenuButton)child).Resources["Disappear"] as Storyboard;

                    storyboard.Begin();
                }
                else if(child is Line)
                {
                    bool isHorizontal = ((Line)child).X1 == -5;

                    Storyboard storyboard = new Storyboard();
                    DoubleAnimation doubleAnimation = new DoubleAnimation();
                    doubleAnimation.EasingFunction = new SineEase() { EasingMode = EasingMode.EaseIn };
                    doubleAnimation.To = -5;
                    doubleAnimation.Duration = new Duration(timeSpanAnimations);
                    doubleAnimation.EnableDependentAnimation = true;
                    storyboard.Children.Add(doubleAnimation);
                    Storyboard.SetTargetProperty(doubleAnimation, isHorizontal ? "X2" : "Y2");
                    Storyboard.SetTarget(doubleAnimation, (Line)child);
                    storyboard.Begin();
                }
            }

            await Task.Delay(timeSpanAnimations);

            ParameterIsSelected?.Invoke(this, button.Value);
        }

        private double BoardLineThickness()
        {
            return Math.Max((2.2f) * (this.ActualWidth / 808), 1.0);
        }

        public void UpdateLocalization()
        {
            foreach (var child in LayoutRoot.Children)
            {
                if (child is MenuButton )
                {
                    ((MenuButton)child).UpdateLocalization();
                }
            }
        }

        public MenuControl(int rowsCount, int columnsCount, LocalizationItem[] texts, string[] icons)
        {
            this.InitializeComponent();
            
            LayoutRoot.ColumnDefinitions.Clear();
            LayoutRoot.RowDefinitions.Clear();


            for (int h = 0; h < rowsCount; h++)
                LayoutRoot.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });


            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.25, GridUnitType.Star) });

            for (int v = 0; v < columnsCount; v++)
                LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.25, GridUnitType.Star) });

            MenuButton menuButton;
            int valuesIndex = 0;
            for (int h = 0; h < rowsCount; h++)
            {
                for (int v = 0; v < columnsCount; v++)
                {
                    menuButton = new MenuButton();
                    menuButton.Pressed += ButtonControl_Pressed;
                    menuButton.IsWhite = false;
                    menuButton.SetValue(Grid.RowProperty, h);
                    menuButton.SetValue(Grid.ColumnProperty, v + 1);
                    menuButton.Value = valuesIndex;
                    menuButton.Text = texts[valuesIndex];
                    menuButton.IconUrl = icons[valuesIndex];
                    menuButton.IsFlipped = false;
                    //menuButton.Diameter = Table.IsFullScreen ? 600 : 300;
                    //menuButton.FontSize = Table.IsFullScreen ? 60 : 40;
                    menuButton.VerticalAlignment = VerticalAlignment.Stretch;
                    menuButton.HorizontalAlignment = HorizontalAlignment.Stretch;
                    menuButton.MarginPercantage = 0.1;

                    LayoutRoot.Children.Add(menuButton);

                    if (isFirst && valuesIndex == 1)
                        humanButton = menuButton;

                    valuesIndex++;
                }
            }

            SizeChanged += MenuControl_SizeChanged;
        }
    }
}
