﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class MenuIconButton : UserControl
    {
        public MenuIconButton()
        {
            this.InitializeComponent();

            (this.Content as FrameworkElement).DataContext = this;

            this.Loaded += MenuIconButton_Loaded;
        }

        private void MenuIconButton_Loaded(object sender, RoutedEventArgs e)
        {
            Storyboard storyboard = this.Resources["PopOut"] as Storyboard;
            storyboard.Begin();
        }

        public event EventHandler Pressed;
        private void Button_Pressed(object sender, System.EventArgs e)
        {
            Storyboard storyboard = this.Resources["Disappear"] as Storyboard;
            storyboard.Begin();
            Pressed?.Invoke(this, e);
        }
    }
}
