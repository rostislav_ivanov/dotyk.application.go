﻿using Common;
using Dotyk.Application.Go.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class GamePanel : UserControl
    {
        private GamePageViewModel _viewModel;

        public GamePanel()
        {
            this.InitializeComponent();
            DataContextChanged += OnDataContextChanged;


            TimerTextBlockPlayer1 = new TextBlock();
            TimerTextBlockPlayer1.FontSize = 60;
            //TimerTextBlockPlayer1.Margin = new Thickness(0, 150, 0, 0);
            TimerTextBlockPlayer1.VerticalAlignment = VerticalAlignment.Center;
            TimerTextBlockPlayer1.HorizontalAlignment = HorizontalAlignment.Center;
            TimerTextBlockPlayer1.RenderTransformOrigin = new Point(0.5, 0.5);
            TimerTextBlockPlayer1.RenderTransform = new RotateTransform() { Angle = -90 };

            TimerTextBlockPlayer2 = new TextBlock();
            TimerTextBlockPlayer2.FontSize = 60;
            //TimerTextBlockPlayer2.Margin = new Thickness(0, 0, 0, 150);
            TimerTextBlockPlayer2.VerticalAlignment = VerticalAlignment.Center;
            TimerTextBlockPlayer2.HorizontalAlignment = HorizontalAlignment.Center;
            TimerTextBlockPlayer2.RenderTransformOrigin = new Point(0.5, 0.5);
            TimerTextBlockPlayer2.RenderTransform = new RotateTransform() { Angle = -90 };

            Loaded += GamePanel_Loaded;

            //SizeChanged += GamePanel_SizeChanged;
        }

        TextBlock TimerTextBlockPlayer1, TimerTextBlockPlayer2;
        private void GamePanel_Loaded(object sender, RoutedEventArgs e)
        {
            //Grid.SetRow(TimerTextBlockPlayer1, 3);
            LayoutRoot.Children.Add(TimerTextBlockPlayer1);

            //Grid.SetRow(TimerTextBlockPlayer2, 2);
            LayoutRoot.Children.Add(TimerTextBlockPlayer2);
        }

        /*
        private void GamePanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double timeSize = TimeRow.ActualHeight;

            if (timeSize > 0 && !Double.IsNaN(timeSize))
            {
                TimerEllipsePlayerBlack.Width = timeSize * 0.647 * 0.9;
                TimerEllipsePlayerBlack.Height = timeSize * 0.647 * 0.9;
                TimerEllipsePlayerBlack.Margin = new Thickness(timeSize * 0.174, timeSize * 0.117, 0, 0);

                TimerTextBlockPlayerBlack.Margin = new Thickness(timeSize * 0.117, timeSize * 0.117, 0, 0);
                TimerTextBlockPlayerBlack.FontSize = timeSize * 0.647 * 0.9;

                TimerEllipsePlayerWhite.Width = timeSize * 0.647 * 0.9;
                TimerEllipsePlayerWhite.Height = timeSize * 0.647 * 0.9;
                TimerEllipsePlayerWhite.Margin = new Thickness(timeSize * 0.174, timeSize * 0.117, 0, timeSize * 0.117);

                TimerTextBlockPlayerWhite.Margin = new Thickness(timeSize * 0.117, timeSize * 0.117, 0, timeSize * 0.117);
                TimerTextBlockPlayerWhite.FontSize = timeSize * 0.647 * 0.9;
            }

            double gridHeight = ActualHeight;

            if (gridHeight > 0 && !Double.IsNaN(gridHeight) && _viewModel !=null)
            {
                _viewModel.FontSizeScore = gridHeight * 0.026 * 0.8;
                _viewModel.FontSizeScoreLabel = gridHeight * 0.0166 * 0.8;

                LayoutRoot.Margin = new Thickness(gridHeight * 0.0311, gridHeight * 0.0555, 0 , gridHeight * 0.0555);
                ScoreGrid.Margin = new Thickness(gridHeight * 0.02 * 0.8, 0, gridHeight * 0.02 * 0.8, 0);
            }
        }
        */

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            _viewModel = DataContext as GamePageViewModel;

            if (_viewModel != null)
            {
                
            }
        }

        

        /*
        public void Pause()
        {
            StopTimers();

            ResignButton.Opacity = 0;
            PassButton.Opacity = 0;
            ResignButton.IsEnabled = false;
            PassButton.IsEnabled = false;
            AbortMoveButton.Visibility = Visibility.Collapsed;
            PauseButton.Text = "Play";
        }

        public void UnPause()
        {
            if (_viewModel.WhoseTurn == 0)
            {
                BouncePlayer1Storyboard.Begin();
            }
            else
            {
                BouncePlayer2Storyboard.Begin();
            }

            ResignButton.Opacity = 1;
            ResignButton.IsEnabled = true;
            PassButton.Opacity = 1;
            PassButton.IsEnabled = true;
            AbortMoveButton.Visibility = Visibility.Visible;
            PauseButton.Text = "Pause";
        }*/

        public event EventHandler BackToMenu;
        public event EventHandler NewGame;

        /*
        public void GameEnded(bool isGameEnded)
        {
            HintButton.Visibility = isGameEnded ? Visibility.Collapsed : Visibility.Visible;
            PauseButton.Visibility = isGameEnded ? Visibility.Collapsed : Visibility.Visible;

            PassButton.Command = isGameEnded ? null : _viewModel.PassCommand;
            PassButton.Text = isGameEnded ? "Menu" : "Pass";
            

            ResignButton.Command = isGameEnded ? null : _viewModel.ResignCommand;
            ResignButton.Text = isGameEnded ? "Play again" : "Resign";

            if (isGameEnded)
            {
                PassButton.Pressed += MenuButton_Pressed;
                ResignButton.Pressed += NewGameButton_Pressed;

                ResignButton.IsEnabled = true;
                PassButton.IsEnabled = true;
                AbortMoveButton.IsEnabled = true;
            }
            else
            {
                PassButton.Pressed -= MenuButton_Pressed;
                ResignButton.Pressed -= NewGameButton_Pressed;
            }
        }*/

        private void NewGameButton_Pressed(object sender, EventArgs e)
        {
            NewGame?.Invoke(null, null);
        }

        private void MenuButton_Pressed(object sender, EventArgs e)
        {
            BackToMenu?.Invoke(null, null);
        }
        
        /*
        public void SetColorAndNames(bool isWhite)
        {
            if (isWhite)
            {
                PlayerBlackNameTextBlock.Text = "Opponent";
                PlayerWhiteNameTextBlock.Text = "You";
            }
            else
            {
                PlayerBlackNameTextBlock.Text = "You";
                PlayerWhiteNameTextBlock.Text = "Opponent";
            }
        }*/
    }
}
