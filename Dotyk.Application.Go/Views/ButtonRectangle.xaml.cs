﻿using Prism.Commands;
using System;
using System.Windows.Input;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class ButtonRectangle : UserControl
    {
        public ButtonRectangle()
        {
            //IsEnabledChanged += ButtonRectangle_IsEnabledChanged;
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
            this.SizeChanged += ButtonRectangle_SizeChanged;

            ButtonText.Foreground = new SolidColorBrush(Colors.Black);
            ButtonEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 233, 168, 99));
        }

        //ToDo: using VisualState
        public void SetEnable(bool isEnable)
        {
            IsEnabled = isEnable;
            Button.IsEnabled = isEnable;

            if (isEnable && Command.CanExecute())
                Enable.Begin();
            else
                Disable.Begin();
        }

        public void HideIfDisable()
        {
            if(!IsEnabled || !Command.CanExecute())
                Hide.Begin();
        }

        public event EventHandler Pressed;
        private void Button_Pressed(object sender, System.EventArgs e)
        {
            Pressed?.Invoke(this, e);
        }

        #region IsWhite
        public bool IsWhite
        {
            get => (bool)GetValue(IsWhiteProperty);
            set
            {
                SetValue(IsWhiteProperty, value);
                if (value)
                {
                    ButtonText.Foreground = new SolidColorBrush(Colors.Black);
                    ButtonEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 220, 220, 220));
                }
                else
                {
                    ButtonText.Foreground = new SolidColorBrush(Colors.White);
                    ButtonEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 70, 70, 70));
                }
            }
        }
        public static readonly DependencyProperty IsWhiteProperty =
            DependencyProperty.Register("IsWhite", typeof(bool), typeof(ButtonRectangle), new PropertyMetadata(null));

        #endregion IsWhite

        #region Text
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ButtonRectangle), new PropertyMetadata(null));

        #endregion Text

        #region Command
        public static DependencyProperty CommandProperty = 
            DependencyProperty.Register("Command", typeof(DelegateCommand), typeof(ButtonRectangle), new PropertyMetadata(null));
        public DelegateCommand Command
        {
            get
            {
                return (DelegateCommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }
        #endregion

        private void ButtonRectangle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!Double.IsNaN(ActualHeight) && !Double.IsNaN(ActualWidth))
            {
                double diameter = Math.Min(ActualWidth, ActualHeight);
                this.Button.Width = diameter;
                this.Button.Height = diameter;
            }

            this.Button.UpdatePosition();
        }

        public void UpdatePosition()
        {
            this.Button.UpdatePosition();
        }
    }
}
