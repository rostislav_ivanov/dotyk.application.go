﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Dotyk.Application.Go.ViewModels;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using System.Globalization;
using Windows.UI.Xaml.Media;
using Windows.UI;
using System.Linq;
using Common;
using Windows.UI.Xaml.Media.Animation;
using EngineAdapter;
using EngineAdapter.Engine;
using Dotyk.Application.Go.Strings;

namespace Dotyk.Application.Go.Views
{
    //ToDo: Binding for everything
    public sealed partial class GamePage
    {
        DispatcherTimer timer1;
        DispatcherTimer timer2;
        private GamePageViewModel _viewModel;

        public event EventHandler BackToMenu;
        public event EventHandler<MessageType> ChangeBackground;

        TimeLocalizationItem timeLocalization;
        public GamePage()
        {
            InitializeComponent();

            SizeChanged += OnSizeChanged;
            DataContextChanged += OnDataContextChanged;
            
            PauseJalousie.AnimationCompleted += PauseJalousie_AnimationCompleted;
            //PauseJalousie.AnimationShowCompleted += PauseJalousie_AnimationCompleted1;

            PassButtonPlayer1.Text = PassButtonPlayer2.Text = new Strings.LocalizationItem("PassButton");
            AbortMoveButtonPlayer1.Text = AbortMoveButtonPlayer2.Text = new Strings.LocalizationItem("AbortButton");
        }

        bool isFlipped;
        private void LayoutForTableSize()
        {
            if (!Table.IsLeftToRight && Table.IsFullScreen && !Table.IsDuos && !Table.IsBar)
            {
                isFlipped = true;
            }
            else
            {
                isFlipped = false;
            }

            if (!Table.isTable)
            {
                //PauseButton.Visibility = Visibility.Visible;
            }

            double boardSize;
            double sideColumnWidth = 120; //min value;
            double rowForHandsHeight = 100;  //min value;

            if (Table.IsDuos)
            {
                //AbortMoveButtonPlayer2.Visibility = Visibility.Visible;
                //PassButtonPlayer2.Visibility = Visibility.Visible;

                rowForHandsHeight = 140;
                boardSize = ActualHeight - 2 * rowForHandsHeight;

                BoardColumn.Width = new GridLength(boardSize);
                LeftColumn.Width = new GridLength(rowForHandsHeight);
                RightColumn.Width = new GridLength(rowForHandsHeight);

                RowForHands2.Height = new GridLength(rowForHandsHeight);
                RowForAbortPlayer2.Height = new GridLength(200 + 8 + 100);
                RowForPassPlayer2.Height = new GridLength(8 + 8 + 100);
                RowForTimerPlayer2.Height = new GridLength(1, GridUnitType.Star);
                RowBetweenTimers.Height = new GridLength(80);
                RowForTimerPlayer1.Height = new GridLength(1, GridUnitType.Star);
                RowForPassPlayer1.Height = new GridLength(8 + 8 + 100);
                RowForAbortPlayer1.Height = new GridLength(200 + 8 + 100);
                RowForHands1.Height = new GridLength(rowForHandsHeight);

                AbortMoveButtonPlayer1.MarginPercantage = 0;
                AbortMoveButtonPlayer1.Margin = new Thickness(0, 8, 0, 200);
                AbortMoveButtonPlayer1.Height = 100;
                PassButtonPlayer1.MarginPercantage = 0;
                PassButtonPlayer1.Height = 100;
                PassButtonPlayer1.Margin = new Thickness(0, 8, 0, 8);
                Grid.SetRow(PassButtonPlayer1, 6);
                Grid.SetRow(AbortMoveButtonPlayer1, 7);

                AbortMoveButtonPlayer2.MarginPercantage = 0;
                AbortMoveButtonPlayer2.Margin = new Thickness(0, 200, 0, 8);
                AbortMoveButtonPlayer2.Height = 100;
                PassButtonPlayer2.MarginPercantage = 0;
                PassButtonPlayer2.Height = 100;
                PassButtonPlayer2.Margin = new Thickness(0, 8, 0, 8);
                Grid.SetRow(PassButtonPlayer2, 2);
                Grid.SetRow(AbortMoveButtonPlayer2, 1);

                Grid.SetColumn(TimerPlayer1, 2);
                Grid.SetRow(TimerPlayer1, 0);
                Grid.SetRowSpan(TimerPlayer1, 4);

                Grid.SetColumn(TimerPlayer2, 2);
                Grid.SetRow(TimerPlayer2, 5);
                Grid.SetRowSpan(TimerPlayer2, 4);

                TimerPlayer1.VerticalAlignment = VerticalAlignment.Bottom;
                TimerPlayer2.VerticalAlignment = VerticalAlignment.Top;
                TimerPlayer1.HorizontalAlignment = HorizontalAlignment.Center;
                TimerPlayer2.HorizontalAlignment = HorizontalAlignment.Center;
                //TimerPlayer1.Margin = new Thickness(15);
                //TimerPlayer2.Margin = new Thickness(15);

                MessagePlayer1.Margin = new Thickness(70, 0, 70, 0);
                MessagePlayer2.Margin = new Thickness(70, 0, 70, 0);
                MessagePlayer1.FontSize = 50;
                MessagePlayer2.FontSize = 50;
                Grid.SetRow(MessagePlayer2, 9);
            }
            else
            {
                if (Table.IsFullScreen)
                {
                    if (_viewModel != null && _viewModel.IsGameWithHuman)
                    {
                        MessagePlayer1.Visibility = Visibility.Visible;
                        AbortMoveButtonPlayer2.Visibility = Visibility.Visible;
                        PassButtonPlayer2.Visibility = Visibility.Visible;
                    }
                    else
                        MessagePlayer1.Visibility = Visibility.Collapsed;

                    rowForHandsHeight = 270;
                    boardSize = ActualHeight - 2*rowForHandsHeight;

                    LeftColumn.Width = isFlipped ? new GridLength(1, GridUnitType.Star) : new GridLength(232);
                    BoardColumn.Width = new GridLength(boardSize);
                    RightColumn.Width = isFlipped ? new GridLength(232) : new GridLength(1, GridUnitType.Star);

                    

                    RowForHands2.Height = new GridLength(rowForHandsHeight);
                    RowForAbortPlayer2.Height = new GridLength(15 + 152 + 200);
                    RowForPassPlayer2.Height = new GridLength(15 + 152 + 15);
                    RowForTimerPlayer2.Height = new GridLength(1, GridUnitType.Star);
                    RowBetweenTimers.Height = new GridLength(300);
                    RowForTimerPlayer1.Height = new GridLength(1, GridUnitType.Star);
                    RowForPassPlayer1.Height = new GridLength(15 + 152 + 15);
                    RowForAbortPlayer1.Height = new GridLength(200 + 152 + 15);
                    RowForHands1.Height = new GridLength(rowForHandsHeight);

                    AbortMoveButtonPlayer1.Margin = new Thickness(40, 15, 40, 200);
                    AbortMoveButtonPlayer1.Height = 152;
                    PassButtonPlayer1.Margin = new Thickness(40, 15, 40, 15);
                    PassButtonPlayer1.Height = 152;
                    Grid.SetRow(PassButtonPlayer1, 6);
                    Grid.SetRow(AbortMoveButtonPlayer1, 7);

                    Grid.SetColumn(PassButtonPlayer1, isFlipped ? 2 : 0);
                    Grid.SetColumn(AbortMoveButtonPlayer1, isFlipped ? 2 : 0);
                    Grid.SetColumn(PassButtonPlayer2, isFlipped ? 2 : 0);
                    Grid.SetColumn(AbortMoveButtonPlayer2, isFlipped ? 2 : 0);

                    AbortMoveButtonPlayer2.Margin = new Thickness(40, 200, 40, 15);
                    AbortMoveButtonPlayer2.Height = 152;
                    PassButtonPlayer2.Margin = new Thickness(40, 15, 40, 15);
                    PassButtonPlayer2.Height = 152;
                    Grid.SetRow(PassButtonPlayer2, 2);
                    Grid.SetRow(AbortMoveButtonPlayer2, 1);

                    Grid.SetColumn(TimerPlayer1, isFlipped ? 0 : 2);
                    Grid.SetRow(TimerPlayer1, 5);
                    Grid.SetRowSpan(TimerPlayer1, 3);

                    Grid.SetColumn(TimerPlayer2, isFlipped ? 0 : 2);
                    Grid.SetRow(TimerPlayer2, 1);
                    Grid.SetRowSpan(TimerPlayer2, 3);

                    TimerPlayer1.VerticalAlignment = VerticalAlignment.Top;
                    TimerPlayer2.VerticalAlignment = VerticalAlignment.Bottom;
                    TimerPlayer1.HorizontalAlignment = isFlipped ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                    TimerPlayer2.HorizontalAlignment = isFlipped ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                    TimerPlayer1.Margin = isFlipped ? new Thickness(0, 0, 50, 0) : new Thickness(50, 0, 0, 0);
                    TimerPlayer2.Margin = isFlipped ? new Thickness(0, 0, 50, 0) : new Thickness(50, 0, 0, 0);

                    TimerPlayer1.RenderTransform = new ScaleTransform() { ScaleY = isFlipped ? -1 : 1 , ScaleX = isFlipped ? -1 : 1 };
                    TimerPlayer2.RenderTransform = new ScaleTransform() { ScaleY = isFlipped ? -1 : 1 , ScaleX = isFlipped ? -1 : 1 };

                    MessagePlayer2.Margin = new Thickness(0, 0, 0, 30);
                    MessagePlayer2.VerticalAlignment = VerticalAlignment.Center;
                    MessagePlayer2.FontSize = 60;
                    Grid.SetRow(MessagePlayer2, 9);
                    Grid.SetRowSpan(MessagePlayer2, 1);
                    Grid.SetColumn(MessagePlayer2, 1);
                }
                else
                {
                    AbortMoveButtonPlayer2.Visibility = Visibility.Collapsed;
                    PassButtonPlayer2.Visibility = Visibility.Collapsed;
                    MessagePlayer1.Visibility = Visibility.Visible;

                    //remove
                    /*
                    AbortMoveButtonPlayer2.Margin = new Thickness(0, 40, 0, 0);
                    AbortMoveButtonPlayer2.Height = 130;
                    PassButtonPlayer2.Margin = new Thickness(0, 140, 0, 0);
                    PassButtonPlayer2.Height = 130;
                    Grid.SetRow(AbortMoveButtonPlayer2, 5);
                    Grid.SetRow(PassButtonPlayer2, 4);
                    Grid.SetColumn(AbortMoveButtonPlayer2, 2);
                    Grid.SetColumn(PassButtonPlayer2, 2);
                    */

                    boardSize = ActualHeight;
                    sideColumnWidth = (ActualWidth - boardSize) / 2;

                    BoardColumn.Width = new GridLength(boardSize);
                    LeftColumn.Width = new GridLength(sideColumnWidth);
                    RightColumn.Width = new GridLength(sideColumnWidth);

                    RowForHands2.Height = new GridLength(0);
                    RowForAbortPlayer2.Height = new GridLength(0.75, GridUnitType.Star);
                    RowForPassPlayer2.Height = new GridLength(0);
                    RowForTimerPlayer2.Height = new GridLength(1, GridUnitType.Auto);
                    RowBetweenTimers.Height = new GridLength(1, GridUnitType.Auto);
                    RowForTimerPlayer1.Height = new GridLength(1, GridUnitType.Auto);
                    RowForAbortPlayer1.Height = new GridLength(1, GridUnitType.Auto);
                    RowForPassPlayer1.Height = new GridLength(1, GridUnitType.Star);
                    RowForHands1.Height = new GridLength(0);

                    AbortMoveButtonPlayer1.Margin = new Thickness(0, 40, 0, 0);
                    AbortMoveButtonPlayer1.Height = 130;
                    PassButtonPlayer1.Margin = new Thickness(0, 170, 0, 0);
                    PassButtonPlayer1.Height = 130;
                    Grid.SetRow(AbortMoveButtonPlayer1, 5);
                    Grid.SetRow(PassButtonPlayer1, 4);

                    Grid.SetColumn(PassButtonPlayer1, 0);
                    Grid.SetColumn(AbortMoveButtonPlayer1,  0);

                    Grid.SetColumn(TimerPlayer1, 0);
                    Grid.SetColumn(TimerPlayer2, 2);
                    Grid.SetRow(TimerPlayer2, 3);
                    Grid.SetRow(TimerPlayer1, 3);
                    Grid.SetRowSpan(TimerPlayer1, 1);
                    Grid.SetRowSpan(TimerPlayer2, 1);

                    TimerPlayer1.VerticalAlignment = VerticalAlignment.Center;
                    TimerPlayer2.VerticalAlignment = VerticalAlignment.Center;
                    TimerPlayer1.HorizontalAlignment = HorizontalAlignment.Center;
                    TimerPlayer2.HorizontalAlignment = HorizontalAlignment.Center;
                    TimerPlayer1.Margin = new Thickness(0, 30, 0, 40);
                    TimerPlayer2.Margin = new Thickness(0, 30, 0, 40);

                    TimerPlayer1.RenderTransform = new ScaleTransform() { ScaleY = 1, ScaleX = 1 };
                    TimerPlayer2.RenderTransform = new ScaleTransform() { ScaleY = 1, ScaleX = 1 };

                    //only for score
                    MessagePlayer1.Margin = new Thickness(15, 0, 15, 0);
                    MessagePlayer1.VerticalAlignment = VerticalAlignment.Top;
                    MessagePlayer1.FontSize = 48;
                    Grid.SetRow(MessagePlayer1, 4);
                    Grid.SetRowSpan(MessagePlayer1, 3);
                    Grid.SetColumn(MessagePlayer1, 0);
                    MessagePlayer1ScaleTransform.ScaleX = 1;
                    MessagePlayer1ScaleTransform.ScaleY = 1;

                    MessagePlayer2.Margin = new Thickness(15, 0, 15, 0);
                    MessagePlayer2.VerticalAlignment = VerticalAlignment.Top;
                    MessagePlayer2.FontSize = 48;
                    Grid.SetRow(MessagePlayer2, 4);
                    Grid.SetRowSpan(MessagePlayer2, 3);
                    Grid.SetColumn(MessagePlayer2, 2);
                }
            }

            TimerPlayer1.Layout();
            TimerPlayer2.Layout();

            BigBoard.Width = boardSize;
            BigBoard.Height = boardSize;
            
            BigBoard.FixPieces();
        }

        private void GameWithHumanOrBot(bool withHuman)
        {
            if(withHuman)
            {
                MessagePlayer1.Visibility = Visibility.Visible;
                PassButtonPlayer2.Visibility = Visibility.Visible;
                AbortMoveButtonPlayer2.Visibility = Visibility.Visible;
            }
            else
            {
                MessagePlayer1.Visibility = Visibility.Collapsed;
                PassButtonPlayer2.Visibility = Visibility.Collapsed;
                AbortMoveButtonPlayer2.Visibility = Visibility.Collapsed;
            }
        }

        private async void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            // Unsubscribe from old viewmodel's events.
            if (_viewModel != null)
                _viewModel.PropertyChanged -= ViewModelOnPropertyChanged;


            _viewModel = DataContext as GamePageViewModel;

            if (_viewModel != null)
            {
                if (timer1 == null)
                {
                    timer1 = new DispatcherTimer();
                    timer1.Interval = new TimeSpan(0, 0, 1);
                    timer1.Tick += Timer_Tick1;
                }

                if (timer2 == null)
                {
                    timer2 = new DispatcherTimer();
                    timer2.Interval = new TimeSpan(0, 0, 1);
                    timer2.Tick += Timer_Tick2;
                }

                _viewModel.PropertyChanged += ViewModelOnPropertyChanged;

                //await AdjustToVm(nameof(GamePageViewModel.WhoseTurn));
                await AdjustToVm(nameof(GamePageViewModel.MessageText));
            }
        }

        private void Timer_Tick1(object sender, object e)
        {
            DecreaseTimeResult isTimeNotFinished = _viewModel.Player1.LeftTime.DecreaseTime();

            TimerPlayer1.TimerText = _viewModel.Player1.LeftTime.TimeFormat();

            if (isTimeNotFinished == DecreaseTimeResult.Buyomi)
            {
                TimerPlayer1.ShowBuyomi(_viewModel.Player1.LeftTime.StonesFormat());
            }
            else if (isTimeNotFinished == DecreaseTimeResult.BuyomiNotStarted)
            {
                TimerPlayer1.ShowBuyomi(timeLocalization.ToString());
            }

            if (isTimeNotFinished == DecreaseTimeResult.Ended)
            {
                _viewModel.ExecuteTimeEnded();
            }
        }

        private void Timer_Tick2(object sender, object e)
        {
            DecreaseTimeResult isTimeNotFinished = _viewModel.Player2.LeftTime.DecreaseTime();

            TimerPlayer2.TimerText = _viewModel.Player2.LeftTime.TimeFormat();

            if(isTimeNotFinished == DecreaseTimeResult.Buyomi)
            {
                TimerPlayer2.ShowBuyomi(_viewModel.Player2.LeftTime.StonesFormat());
            }
            else if (isTimeNotFinished == DecreaseTimeResult.BuyomiNotStarted)
            {
                TimerPlayer2.ShowBuyomi(timeLocalization.ToString());
            }

            if (isTimeNotFinished == DecreaseTimeResult.Ended)
            {
                _viewModel.ExecuteTimeEnded();
            }
        }

        private async void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            await AdjustToVm(propertyChangedEventArgs.PropertyName);
        }

        bool startTimer;
        private async Task AdjustToVm(string propertyName)
        {
            if (_viewModel == null)
                return;

            switch (propertyName)
            {
                case nameof(GamePageViewModel.IsLoaded):

                    this.BigBoard.IsGameWithHuman = _viewModel.IsGameWithHuman;
                    GameWithHumanOrBot(_viewModel.IsGameWithHuman);

                    SetColors();

                    LoadTimers(_viewModel.WhoseTurn == 0);
                    break;
                case nameof(GamePageViewModel.WhoseTurn):
                    if (_viewModel.Status == EngineAdapter.Engine.GoGameStatus.Active)
                    {
                        /*if (!startTimer)
                        {
                            if (_viewModel.IsPlayer1Active)
                                timer1.Start();
                            else
                                timer2.Start();
                            startTimer = true;
                        }*/

                        Move(_viewModel.IsPlayer1Active);
                    }
                    else
                    {
                        StopTimers();
                        UpdateButtons(_viewModel.IsPlayer1Active);
                    }
                    break;
                case nameof(GamePageViewModel.IsPaused):
                    if (_viewModel.IsPaused)
                    {
                        Pause();
                    }
                    else
                    {
                        UnPause();
                    }
                    break;
                case nameof(GamePageViewModel.IsCalculatingResults):
                    if(_viewModel.IsCalculatingResults)
                    {
                        StopTimers();
                        UpdateButtons(_viewModel.IsPlayer1Active);
                    }
                    break;
                case nameof(GamePageViewModel.Status):
                    if (_viewModel.Status != EngineAdapter.Engine.GoGameStatus.NotStarted &&
                        _viewModel.Status != EngineAdapter.Engine.GoGameStatus.Active)
                    {
                        //FirstPlaerPanel.GameEnded(true);
                    }
                    else
                    {
                        //FirstPlaerPanel.GameEnded(false);
                    }
                    break;
                case nameof(GamePageViewModel.MessageText):
                    UpdateMessage();
                    break;
            }
        }

        private void UpdateMessage()
        {
            ChangeBackground?.Invoke(null, _viewModel.MessageType);

            TimerPlayer1.NewMessage(_viewModel.MessageType);
            TimerPlayer2.NewMessage(_viewModel.MessageType);

            switch (_viewModel.MessageType)
            {
                case MessageType.BlackWins:
                    MessagePlayer1.Foreground = Resources["WhiteBrush"] as SolidColorBrush;
                    MessagePlayer2.Foreground = Resources["WhiteBrush"] as SolidColorBrush;
                    SetScoreMessage();
                    break;
                case MessageType.WhiteWins:
                    MessagePlayer1.Foreground = Resources["BlackBrush"] as SolidColorBrush;
                    MessagePlayer2.Foreground = Resources["BlackBrush"] as SolidColorBrush;
                    SetScoreMessage();
                    break;
                case MessageType.Error:
                case MessageType.Simple:
                    MessagePlayer1.Foreground = Resources["WhiteBrush"] as SolidColorBrush;
                    MessagePlayer2.Foreground = Resources["WhiteBrush"] as SolidColorBrush;
                    MessagePlayer1.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
                    MessagePlayer2.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
                    break;
            }

            MessagePlayer1.Visibility = 
                _viewModel.IsGameWithHuman || !Table.IsFullScreen && (_viewModel.Status == GoGameStatus.Ended) ?
                Visibility.Visible : Visibility.Collapsed;
        }

        private void SetScoreMessage()
        {
            if (_viewModel.Status != GoGameStatus.Ended)
            {
                MessagePlayer1.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
                MessagePlayer2.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
            }
            else
            {
                var PlayerWhite = _viewModel.PlayerWhite;
                var PlayerBlack = _viewModel.PlayerBlack;

                string delimeter = Table.IsFullScreen ? " " : "\r\n";
                string whiteScoreText = string.Concat(new LocalizationItem("White"), ":", delimeter,
                                                          PlayerWhite.Komi, " ", new LocalizationItem("Komi"), delimeter, "+ ",
                                                          PlayerWhite.Prisoners, " ", new LocalizationItem("Prisoners"), delimeter, "+ ",
                                                          PlayerWhite.Area - PlayerWhite.Alive, " ", new LocalizationItem("Territory"), delimeter, "= ",
                                                          PlayerWhite.ScoreJapanese, ".");

                string blackScoreText = string.Concat(new LocalizationItem("Black"), ":", delimeter,
                                                          PlayerBlack.Prisoners, " ", new LocalizationItem("Prisoners"), delimeter, "+ ",
                                                          PlayerBlack.Area - PlayerBlack.Alive, " ", new LocalizationItem("Territory"), delimeter, "= ",
                                                          PlayerBlack.ScoreJapanese, ".");

                string whoWins = new LocalizationItem(_viewModel.MessageType == MessageType.BlackWins ? "BlackWinsBy" : "WhiteWinsBy").ToString();
                if (Table.IsFullScreen)
                {
                    MessagePlayer1.Text = MessagePlayer2.Text = string.Concat(whiteScoreText, "\r\n", blackScoreText, "\r\n", whoWins);
                }
                else
                {
                    MessagePlayer1.Text = PlayerWhite.PlayerType == PlayerType.Human ? whiteScoreText : blackScoreText;
                    MessagePlayer2.Text = string.Concat(PlayerWhite.PlayerType == PlayerType.Human ? blackScoreText : whiteScoreText, "\r\n\r\n", whoWins);
                }
            }
        }

        //ToDo: Binding
        private void SetColors()
        {
            PauseButton.ButtonControl.Command = _viewModel.PauseCommand;

            bool isPlayer1IsBlack = _viewModel.ActiveGame.IsPlayer1IsBlack;

            TimerPlayer1.IsWhite = !isPlayer1IsBlack;
            TimerPlayer2.IsWhite = isPlayer1IsBlack;
        }

        private void MenuButton_Pressed(object sender, EventArgs e)
        {
            BackToMenu?.Invoke(null, null);
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            if (!Double.IsNaN(ActualHeight))
            {
                LayoutForTableSize();
                UpdateMessage();
            }
        }

        public void LoadTimers(bool isFirstMoveBlack)
        {
            timeLocalization = new TimeLocalizationItem(_viewModel.Player1.LeftTime, false); //ToDo: I assume that there are the same time limits for both players at the beginning

            if (isFirstMoveBlack)
            {
                TimerPlayer1.Run();
                TimerPlayer2.Stop();
            }
            else
            {
                TimerPlayer1.Stop();
                TimerPlayer2.Run();
            }

            TimerPlayer1.TimerText = _viewModel.Player1.LeftTime.TimeFormat();
            TimerPlayer2.TimerText = _viewModel.Player2.LeftTime.TimeFormat();

            if (_viewModel.Player1.LeftTime.InitialStones > 0 && _viewModel.Player1.LeftTime.MainTime <= 0)
            {
                TimerPlayer1.ShowBuyomi(_viewModel.Player1.LeftTime.StonesFormat());
            }
            else if(_viewModel.Player1.LeftTime.InitialStones > 0)
            {
                TimerPlayer1.ShowBuyomi(timeLocalization.ToString());
            }

            if (_viewModel.Player2.LeftTime.InitialStones > 0 && _viewModel.Player2.LeftTime.MainTime <= 0)
            {
                TimerPlayer2.ShowBuyomi(_viewModel.Player2.LeftTime.StonesFormat());
            }
            else if (_viewModel.Player2.LeftTime.InitialStones > 0)
            {
                TimerPlayer2.ShowBuyomi(timeLocalization.ToString());
            }
        }

        public void StopTimers()
        {
            timer1.Stop();
            timer2.Stop();

            TimerPlayer1.StopButVisible();
            TimerPlayer2.StopButVisible();
        }

        private void UpdateButtons(bool isPlayer1Active)
        {
            PassButtonPlayer1.SetEnable(isPlayer1Active, _viewModel.MessageType == MessageType.BlackWins || _viewModel.MessageType == MessageType.WhiteWins);
            AbortMoveButtonPlayer1.SetEnable(!_viewModel.IsGameWithHuman || !isPlayer1Active, _viewModel.MessageType == MessageType.BlackWins || _viewModel.MessageType == MessageType.WhiteWins);

            PassButtonPlayer2.SetEnable(!isPlayer1Active, _viewModel.MessageType == MessageType.BlackWins || _viewModel.MessageType == MessageType.WhiteWins);
            AbortMoveButtonPlayer2.SetEnable(isPlayer1Active, _viewModel.MessageType == MessageType.BlackWins || _viewModel.MessageType == MessageType.WhiteWins);
        }

        public void Move(bool isPlayer1Active)
        {
            UpdateButtons(isPlayer1Active);
            TimerPlayer1.PrisonersText = _viewModel.Player1.Prisoners.ToString();
            TimerPlayer2.PrisonersText = _viewModel.Player2.Prisoners.ToString();

            if (isPlayer1Active)
            {
                TimerPlayer1.Run();
                TimerPlayer2.Stop();
                TimerPlayer2.TimerText = _viewModel.Player2.LeftTime.TimeFormat();
                if(_viewModel.Player2.LeftTime.MainTime <= 0)
                    TimerPlayer2.BuyomiText = _viewModel.Player2.LeftTime.StonesFormat();

                timer1.Start();
                timer2.Stop();
            }
            else
            {
                TimerPlayer1.Stop();
                TimerPlayer2.Run();
                TimerPlayer1.TimerText = _viewModel.Player1.LeftTime.TimeFormat();
                if (_viewModel.Player1.LeftTime.MainTime <= 0)
                    TimerPlayer1.BuyomiText = _viewModel.Player1.LeftTime.StonesFormat();

                timer1.Stop();
                timer2.Start();
            }

            startTimer = true;
        }

        public void Pause()
        {
            StopTimers();
            PauseJalousie.Visibility = Visibility.Visible;
            PauseJalousie.Show();
            PauseButton.Text = new Strings.LocalizationItem("Play");
        }

        public void UnPause()
        {
            PauseJalousie.Hide();
            PauseButton.Text = new Strings.LocalizationItem("Pause");
        }

        private void PauseJalousie_AnimationCompleted(object sender, EventArgs e)
        {
            //PauseJalousie.Visibility = Visibility.Collapsed;
            if (_viewModel.Status == GoGameStatus.Active)
            {
                if (_viewModel.IsPlayer1Active)
                {
                    TimerPlayer1.Run();
                    TimerPlayer2.Stop();
                    timer1.Start();
                }
                else
                {
                    TimerPlayer1.Stop();
                    TimerPlayer2.Run();
                    timer2.Start();
                }


                startTimer = true;
            }
        }

        public void UpdateLocalization()
        {
            PauseButton.UpdateLocalization();
            PassButtonPlayer1.UpdateLocalization();
            AbortMoveButtonPlayer1.UpdateLocalization();
            PassButtonPlayer2.UpdateLocalization();
            AbortMoveButtonPlayer2.UpdateLocalization();

            if (_viewModel.MessageType == MessageType.BlackWins || _viewModel.MessageType == MessageType.WhiteWins)
                SetScoreMessage();
            else
            {
                MessagePlayer1.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
                MessagePlayer2.Text = _viewModel.MessageText == null ? "" : _viewModel.MessageText.ToString();
            }
        }
    }
}
