﻿using Common;
using Dotyk.Application.Go.Model;
using Dotyk.Application.Go.Strings;
using EngineAdapter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class GameSettings : UserControl
    {
        private class SettingsRow
        {
            public static Grid LayoutRoot;
            public static bool isGameWithHuman;

            public int SelectedValue;
            MenuButton[] menuButtons;
            MenuButton[][] menuButtons2;

            public SettingsRow(ToStringClass[] valuesRow, LocalizationItem rowHeader, int selectedIndex, int[] rowNumbers, bool isColor = false)
            {
                this.isColor = isColor;

                if (rowNumbers.Length == 2)
                {
                    menuButtons2 = new MenuButton[valuesRow.Length][];

                    for (int i = 0; i < valuesRow.Length; i++)
                    {
                        menuButtons2[i] = new MenuButton[2];
                        menuButtons2[i][0] = CreateComboxItem(rowNumbers[0], valuesRow.Length - i + 1, i, valuesRow[i], i == selectedIndex, true);
                        menuButtons2[i][1] = CreateComboxItem(rowNumbers[1], i + 2, i, valuesRow[i], i == selectedIndex);
                    }

                    CreateComboxHeader(rowNumbers[0], rowHeader, true);
                    CreateComboxHeader(rowNumbers[1], rowHeader);
                }
                else
                {
                    menuButtons = new MenuButton[valuesRow.Length];
                    for (int i = 0; i < valuesRow.Length; i++)
                    {
                        menuButtons[i] = CreateComboxItem(rowNumbers[0], i + 2, i, valuesRow[i], i == selectedIndex);
                    }

                    CreateComboxHeader(rowNumbers[0], rowHeader);
                }

                SelectedValue = selectedIndex;
            }

            private bool isColor;
            private void MenuButton_Pressed(object sender, EventArgs e)
            {
                int newSelectedValue = (sender as MenuButton).Value;
                if (!isGameWithHuman)
                {
                    menuButtons[SelectedValue].IsSelected = false;
                    menuButtons[newSelectedValue].IsSelected = true;
                    SelectedValue = newSelectedValue;
                }
                else if(isColor)
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        menuButtons2[i][0].IsSelected = false;
                        menuButtons2[i][1].IsSelected = false;
                    }

                    MenuButton touchedMenubutton = sender as MenuButton;

                    if (newSelectedValue != 2)
                    {
                        if (touchedMenubutton == menuButtons2[newSelectedValue][0])
                        {
                            menuButtons2[1 - newSelectedValue][1].IsSelected = true;
                            SelectedValue = 1 - newSelectedValue;
                        }
                        else
                        {
                            menuButtons2[1 - newSelectedValue][0].IsSelected = true;
                            SelectedValue = newSelectedValue;
                        }
                        touchedMenubutton.IsSelected = true;
                    }
                    else
                    {
                        menuButtons2[newSelectedValue][0].IsSelected = true;
                        menuButtons2[newSelectedValue][1].IsSelected = true;
                        SelectedValue = newSelectedValue;
                    }

                }
                else
                {
                    menuButtons2[SelectedValue][0].IsSelected = false;
                    menuButtons2[SelectedValue][1].IsSelected = false;
                    
                    menuButtons2[newSelectedValue][0].IsSelected = true;
                    menuButtons2[newSelectedValue][1].IsSelected = true;
                    SelectedValue = newSelectedValue;
                }
            }

            private MenuButton CreateComboxItem(int rowIndex, int columnIndex, int value, ToStringClass text, bool isSelected, bool isFlipped = false)
            {
                MenuButton menuButton = new MenuButton();
                menuButton.VerticalAlignment = VerticalAlignment.Center;
                menuButton.HorizontalAlignment = HorizontalAlignment.Center;
                menuButton.Margin = new Thickness(0, 15, 0, 15);
                menuButton.Text = text;
                menuButton.Value = value;
                menuButton.Pressed += MenuButton_Pressed;
                menuButton.IsSelected = isSelected;
                menuButton.Width = 160;
                menuButton.Height = 160;
                menuButton.MarginPercantage = 0;
                menuButton.UseTouchAnimation = false;

                Grid.SetColumn(menuButton, columnIndex);
                Grid.SetRow(menuButton, rowIndex);

                menuButton.IsFlipped = isFlipped;

                LayoutRoot.Children.Add(menuButton);

                return menuButton;
            }

            private void CreateComboxHeader(int rowNumber, LocalizationItem text, bool isFlipped = false)
            {
                TextBlock rowHeaderTextBlock = new TextBlock();
                rowHeaderTextBlock.HorizontalAlignment = HorizontalAlignment.Left;
                rowHeaderTextBlock.VerticalAlignment = VerticalAlignment.Center;
                rowHeaderTextBlock.FontFamily = new FontFamily("ms-appx:///Assets/GothaProBla.otf");
                rowHeaderTextBlock.Text = text.ToString();
                rowHeaderTextBlock.Tag = text;
                rowHeaderTextBlock.Foreground = new SolidColorBrush(Colors.Black);
                rowHeaderTextBlock.FontSize = 33;
                rowHeaderTextBlock.FontWeight = FontWeights.Bold;
                Grid.SetColumn(rowHeaderTextBlock, 1);
                Grid.SetRow(rowHeaderTextBlock, rowNumber);

                if (isFlipped)
                {
                    rowHeaderTextBlock.RenderTransformOrigin = new Point(0.5, 0.5);
                    rowHeaderTextBlock.RenderTransform = new ScaleTransform() { ScaleX = -1, ScaleY = -1 };
                }

                LayoutRoot.Children.Add(rowHeaderTextBlock);
            }
        }

        public GameSettings(bool isGameWithBot)
        {
            this.InitializeComponent();

            if (isGameWithBot)
            {
                SettingsRow.isGameWithHuman = false;
                CreatePlayWithBotSettings();
            }
            else
            {
                SettingsRow.isGameWithHuman = true;
                CreatePlayWithHumanSettings();
            }

            this.RenderTransformOrigin = new Point(0.5, 0.5);
            this.SizeChanged += GameSettings_SizeChanged;
        }

        private void GameSettings_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(!Table.IsLeftToRight && Table.isTable && Table.IsFullScreen && !Table.IsDuos)
            {
                LayoutRoot.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);
                LayoutRoot.ColumnDefinitions[LayoutRoot.ColumnDefinitions.Count - 1].Width = new GridLength(200);
            }
            else
            {
                LayoutRoot.ColumnDefinitions[0].Width = new GridLength(200);
                LayoutRoot.ColumnDefinitions[LayoutRoot.ColumnDefinitions.Count - 1].Width = new GridLength(1, GridUnitType.Star);
            }
        }

        SettingsRow boardSizeRow;
        SettingsRow botLevelRow;
        SettingsRow timeRow;
        SettingsRow colorRow;

        public void CreatePlayWithBotSettings()
        {
            LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            for(int i=0; i<5; i++)
                LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });

            LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(200) });
            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(380) });
            for (int i = 0; i < 6; i++)
                LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            SettingsRow.LayoutRoot = this.LayoutRoot;

            SetLocalization();
            /*
            boardSizeRow = new SettingsRow(new string[3] {"9x9", "13x13", "19x19" }, 1, "Board size", 2);
            botLevelRow =  new SettingsRow(new string[5] { "25 KYU\r\n(easy)", "19 KYU", "13 KYU", "7 KYU", "3 KYU\r\n(hard)" }, 2, "AI Player", 2);
            timeRow =      new SettingsRow(new string[6] { "30 MIN", "15 MIN", "30 SEC\r\nPER\r\nTURN", "30m +10m\r\nFOR 25\r\nSTONES",
                                                           "+10 min\r\nFOR 25\r\nSTONES", "30s +30s\r\nFOR 5\r\nSTONES" }, 3, "Time Control", 0);
            colorRow =     new SettingsRow(new string[3] { "BLACK", "WHITE", "RANDOM" }, 4, "Color", 2);
            */

            startButton1 = CreateStartButton(5);

        }

        public void CreatePlayWithHumanSettings()
        {
            LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            for (int i = 0; i < 4; i++)
                LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });

            LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(350) });

            for (int i = 0; i < 4; i++)
                LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });

            LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(200) });
            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(380) });
            for (int i = 0; i < 6; i++)
                LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });

            LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            SettingsRow.LayoutRoot = this.LayoutRoot;

            SetLocalization();
            /*boardSizeRow = new SettingsRow(new string[3] { "9x9", "13x13", "19x19" }, 4, 6, "Board size", 2);
            timeRow =      new SettingsRow(new string[6] { "30 MIN", "15 MIN", "30 SEC\r\nPER\r\nTURN", "30m +10m\r\nFOR 25\r\nSTONES",
                                                           "+10 min\r\nFOR 25\r\nSTONES", "30s +30s\r\nFOR 5\r\nSTONES" }, 3, 7, "Time Control", 0);
            colorRow =     new SettingsRow(new string[3] { "BLACK", "WHITE", "RANDOM" }, 2, 8, "Color", 2, true);
            */
            startButton1 = CreateStartButton(1, true);
            startButton2 = CreateStartButton(9);
        }

        public void SetLocalization()
        {
            boardSizeRow = new SettingsRow(new LocalizationItem[3] {
                new LocalizationItem("BoardSizeValue", 9),
                new LocalizationItem("BoardSizeValue", 13),
                new LocalizationItem("BoardSizeValue", 19) }, new LocalizationItem("BoardSize"), 2, SettingsRow.isGameWithHuman ? new int[] { 4, 6 } : new int[] { 1 });

            timeRow = new SettingsRow(defaultTimeLimits, new LocalizationItem("TimeControl"), 0, SettingsRow.isGameWithHuman ? new int[] { 3, 7 } : new int[] {3});

            colorRow = new SettingsRow(new LocalizationItem[3] {
                new LocalizationItem("Black"),
                new LocalizationItem("White"),
                new LocalizationItem("Random") }, new LocalizationItem("Color"), 2, SettingsRow.isGameWithHuman ? new int[] { 2, 8 } : new int[] { 4 }, true);

            if(!SettingsRow.isGameWithHuman)
                botLevelRow = new SettingsRow(new LocalizationItem[5] {
                    new LocalizationItem("BotLevelEasy", 25),
                    new LocalizationItem("BotLevel", 19),
                    new LocalizationItem("BotLevel", 13),
                    new LocalizationItem("BotLevel", 7),
                    new LocalizationItem("BotLevelHard", 3) }, new LocalizationItem("BotName"), 2, new int[] { 2 });
        }

        public void UpdateLocalization()
        {
            foreach (var child in LayoutRoot.Children)
            {
                if (child is MenuButton)
                {
                    ((MenuButton)child).UpdateLocalization();
                }
                else if (child is TextBlock)
                {
                    ((TextBlock)child).Text = ((LocalizationItem)((TextBlock)child).Tag).ToString();
                }
            }
        }

        MenuButton startButton1, startButton2;
        private MenuButton CreateStartButton(int row, bool isFlipped = false)
        {
            MenuButton startButton = new MenuButton();
            Grid.SetColumn(startButton, 2);
            Grid.SetRow(startButton, row);
            startButton.IsWhite = true;
            startButton.Text = new LocalizationItem("StartButton");
            startButton.VerticalAlignment = VerticalAlignment.Center;
            startButton.HorizontalAlignment = HorizontalAlignment.Center;
            startButton.Margin = new Thickness(0, 15, 0, 15);
            startButton.Width = 150;
            startButton.Height = 150;
            startButton.MarginPercantage = 0;
            startButton.Pressed += StartButton_Pressed;
            startButton.UseTouchAnimation = false;

            startButton.IsFlipped = isFlipped;

            LayoutRoot.Children.Add(startButton);

            return startButton;
        }

        private TimeLocalizationItem[] defaultTimeLimits = new TimeLocalizationItem[]
        {
            new TimeLocalizationItem(new TimeLimit(1800, 0, 0)),
            new TimeLocalizationItem(new TimeLimit(900, 0, 0)),
            new TimeLocalizationItem(new TimeLimit(0, 15, 1)),
            new TimeLocalizationItem(new TimeLimit(600, 60, 7)),
            new TimeLocalizationItem(new TimeLimit(0, 60, 5)),
            new TimeLocalizationItem(new TimeLimit(300, 15, 3))
        };

        public event EventHandler Start;
        private void StartButton_Pressed(object sender, EventArgs e)
        {
            switch (boardSizeRow.SelectedValue)
            {
                case 0:
                    God.GameParameters.BoardSize = 9;
                    break;
                case 1:
                    God.GameParameters.BoardSize = 13;
                    break;
                case 2:
                    God.GameParameters.BoardSize = 19;
                    break;
            }

            if (botLevelRow != null)
            {
                switch (botLevelRow.SelectedValue)
                {
                    case 0:
                        God.GameParameters.BotLevel = 2;
                        break;
                    case 1:
                        God.GameParameters.BotLevel = 4;
                        break;
                    case 2:
                        God.GameParameters.BotLevel = 6;
                        break;
                    case 3:
                        God.GameParameters.BotLevel = 8;
                        break;
                    case 4:
                        God.GameParameters.BotLevel = 10;
                        break;
                }
            }

            God.GameParameters.TimeLimit = defaultTimeLimits[timeRow.SelectedValue].TimeLimit;

            switch (colorRow.SelectedValue)
            {
                case 0:
                    God.GameParameters.IsBlack = true;
                    break;
                case 1:
                    God.GameParameters.IsBlack = false;
                    break;
                case 2:
                    God.GameParameters.IsBlack = (new Random(DateTime.Now.Millisecond)).Next() % 2 == 0;
                    break;
            }

            Start?.Invoke(null, null);
        }
    }
}
