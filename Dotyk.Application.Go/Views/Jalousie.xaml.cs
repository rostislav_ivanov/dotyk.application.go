﻿using Common;
using Microsoft.Graphics.Canvas.Effects;
using System;
using System.Numerics;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class Jalousie : UserControl
    {
        ContainerVisual container;
        Compositor _compositor;

        public TimeSpan animationDuration = TimeSpan.FromSeconds(0.5);
        Image pauseImage;

        public Jalousie()
        {
            InitializeComponent();

            SizeChanged += Jalousie_SizeChanged;

            _compositor = ElementCompositionPreview.GetElementVisual(LayoutRoot).Compositor;
            container = _compositor.CreateContainerVisual();

            ElementCompositionPreview.SetElementChildVisual(LayoutRoot, container);

            pauseImage = new Image()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Opacity = 0,
                Source = new BitmapImage(new Uri("ms-appx:///Assets/Pause2.png")),
                Width = 400,
                Height = 400,
                Name= "PauseTextBlock"
            };

            _imageGlassBrush = _compositor.CreateSurfaceBrush();
            _imageGlassBrush.Stretch = CompositionStretch.UniformToFill;

            LoadedImageSurface _imageGlassSurface = LoadedImageSurface.StartLoadFromUri(new Uri("ms-appx:///Assets/Pause glass texture.png"));
            _imageGlassBrush.Surface = _imageGlassSurface;

        }

        CompositionSurfaceBrush _imageGlassBrush;
        private void Jalousie_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (ActualHeight != 0 && !Double.IsNaN(ActualWidth))
            {
                CreateJalousie();
            }
        }

        private Vector3 SetRandomOffset()
        {
            int rand = (new Random(DateTime.Now.Millisecond)).Next() % 4;

            switch(rand)
            {
                case 0:
                    return new Vector3(-(float)ActualWidth, 0, 0);
                case 1:
                    return new Vector3(0, -(float)ActualHeight, 0);
                case 2:
                    return new Vector3((float)ActualWidth, 0, 0);
                case 3:
                    return new Vector3(0, (float)ActualHeight, 0);
                default:
                    return new Vector3(0, -(float)ActualHeight, 0);
            }
        }

        SpriteVisual rect;
        private void CreateJalousie()
        {
            if (container.Children.Count > 0)
            {
                container.Children.RemoveAll();
            }

            GaussianBlurEffect blurEffect = new GaussianBlurEffect()
            {
                Name = "Blur",
                BlurAmount = 10.0f,
                BorderMode = EffectBorderMode.Soft,
                Optimization = EffectOptimization.Speed
            };

            BlendEffect glassEffect = new BlendEffect
            {
                Mode = BlendEffectMode.Overlay
            };

            blurEffect.Source = new CompositionEffectSourceParameter("source");
            CompositionEffectFactory blurEffectFactory = _compositor.CreateEffectFactory(blurEffect);
            CompositionEffectBrush blurBrush = blurEffectFactory.CreateBrush();
            CompositionBackdropBrush backdropBrush = _compositor.CreateBackdropBrush();
            blurBrush.SetSourceParameter("source", backdropBrush);

            glassEffect.Foreground= new CompositionEffectSourceParameter("foreground");
            glassEffect.Background = new CompositionEffectSourceParameter("background");
            CompositionEffectFactory glassEffectFactory = _compositor.CreateEffectFactory(glassEffect);
            CompositionEffectBrush glassBrush = glassEffectFactory.CreateBrush();
            glassBrush.SetSourceParameter("background", _imageGlassBrush);
            glassBrush.SetSourceParameter("foreground", blurBrush);

            Vector3 offset = SetRandomOffset();

            rect = _compositor.CreateSpriteVisual();
            rect.Size = new Vector2((float)ActualWidth, (float)ActualHeight);
            rect.Brush = glassBrush;

            rect.Offset = offset;

            container.Children.InsertAtTop(rect);
        }

        public void Hide()
        {
            var _batch = _compositor.CreateScopedBatch(CompositionBatchTypes.Animation);

            Vector3 offset = SetRandomOffset();

            Vector3KeyFrameAnimation rotateAnimation = _compositor.CreateVector3KeyFrameAnimation();
            rotateAnimation.Target = nameof(Visual.Offset);
            rotateAnimation.Duration = animationDuration;
            rotateAnimation.InsertKeyFrame(1.0f, offset);
            rect.StartAnimation("Offset", rotateAnimation);

            _batch.End();
            _batch.Completed += _batch_Completed;

            if (MainRoot.Children.Contains(pauseImage))
                (Resources["HidePauseText"] as Storyboard).Begin();

        }


        public event EventHandler AnimationCompleted;
        private void _batch_Completed(object sender, CompositionBatchCompletedEventArgs args)
        {
            AnimationCompleted?.Invoke(null, null);
            if (MainRoot.Children.Contains(pauseImage))
                MainRoot.Children.Remove(pauseImage);
        }

        public void Show()
        {
            var _batch = _compositor.CreateScopedBatch(CompositionBatchTypes.Animation);

            Vector3KeyFrameAnimation rotateAnimation = _compositor.CreateVector3KeyFrameAnimation();
            rotateAnimation.Target = nameof(Visual.Offset);
            rotateAnimation.Duration = animationDuration;
            rotateAnimation.InsertKeyFrame(1.0f, new Vector3(0, 0, 0));
            rect.StartAnimation("Offset", rotateAnimation);

            _batch.End();
            _batch.Completed += _batch_Completed1;

            if (!MainRoot.Children.Contains(pauseImage))
                MainRoot.Children.Add(pauseImage);
            (Resources["ShowPauseText"] as Storyboard).Begin();
        }

        public event EventHandler AnimationShowCompleted;
        private void _batch_Completed1(object sender, CompositionBatchCompletedEventArgs args)
        {
            AnimationShowCompleted?.Invoke(null, null);
        }
    }
}
