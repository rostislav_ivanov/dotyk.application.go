﻿using Common;
using Dotyk.Application.Go.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class Timer : UserControl
    {
        public Timer()
        {
            this.InitializeComponent();
            Layout();
        }

        
        private int ringSize(bool isBig = false)
        {
            if(isBig)
                return Table.IsDuos ? 70 : Table.IsFullScreen ? 122 : 98;
            else
                return Table.IsDuos ? 48 : Table.IsFullScreen ? 85 : 68;
        }

        public void Layout()
        {
            if (Table.IsDuos)
            {
                TimerLayoutTransform.LayoutTransform = new RotateTransform() { Angle = 90 };
                TimerStackPanel.Orientation = Orientation.Horizontal;

                if (IsPlayer1)
                {
                    TimerStackPanel.Children.Remove(PrisonersGrid);
                    TimerStackPanel.Children.Remove(TimerTextBlock);

                    TimerStackPanel.Children.Add(TimerTextBlock);
                    TimerStackPanel.Children.Add(PrisonersGrid);

                    Grid.SetColumn(PrisonersColorEllipse, 0);
                    Grid.SetColumn(RingMove, 0);
                    Grid.SetColumn(PrisonersTextBlock, 1);

                    PrisonersTextBlock.Margin = new Thickness(15, 0, 0, 0);
                }
                else
                {
                    Grid.SetColumn(PrisonersColorEllipse, 1);
                    Grid.SetColumn(RingMove, 1);
                    Grid.SetColumn(PrisonersTextBlock, 0);

                    PrisonersTextBlock.Margin = new Thickness(0, 0, 15, 0);
                }

                TimerTextBlock.Margin = new Thickness(30, 0, 30, 0);
                BuyomiTextBlock.RenderTransform = null;

                PrisonersColorEllipse.Width = PrisonersColorEllipse.Height = ringSize();
                RingMove.Width = RingMove.Height = ringSize(true);
                PrisonersTextBlock.FontSize = 28;

                TimerTextBlock.FontSize = 90;
                BuyomiTextBlock.FontSize = 28;

                RingMove.StrokeThickness = 6;
            }
            else
            {
                if (Table.IsFullScreen)
                {
                    TimerLayoutTransform.LayoutTransform = new RotateTransform() { Angle = 90 };

                    PrisonersColorEllipse.Width = PrisonersColorEllipse.Height = ringSize();
                    RingMove.Width = RingMove.Height = ringSize(true);
                    PrisonersTextBlock.FontSize = 47;

                    TimerTextBlock.FontSize = 148;
                    BuyomiTextBlock.FontSize = 30;
                }
                else
                {
                    TimerLayoutTransform.LayoutTransform = new RotateTransform() { Angle = 0 };

                    PrisonersColorEllipse.Width = PrisonersColorEllipse.Height = ringSize();
                    RingMove.Width = RingMove.Height = ringSize(true);
                    PrisonersTextBlock.FontSize = 40;

                    TimerTextBlock.FontSize = 120;
                    BuyomiTextBlock.FontSize = 21;
                }

                TimerTextBlock.Margin = new Thickness(0, 30, 0, 30);
                PrisonersGrid.Height = ringSize(true);
            }
        }

        public void Run()
        {
            RunAnimation.Begin();
        }

        public void StopButVisible()
        {
            StopButVisibleAnimation.Begin();

            if(!isRingSpecial)
                RingMove.Opacity = 0;
        }

        public void Stop()
        {
            StopAnimation.Begin();
        }

        bool isRingSpecial;
        public void NewMessage(MessageType type)
        {
            switch (type)
            {
                case MessageType.BlackWins:
                    PrisonersTextBlock.Foreground = Resources["WhiteBrush"] as SolidColorBrush;
                    TimerTextBlock.Foreground = BuyomiTextBlock.Foreground = Resources["TreeBrush"] as SolidColorBrush;

                    if (!IsWhite)
                    {
                        RingMove.Stroke = Resources["WhiteBrush"] as SolidColorBrush;
                        RingMove.Width = RingMove.Height = ringSize();
                        RingMove.StrokeThickness = 4;
                        RingMove.Opacity = 1;
                        isRingSpecial = true;
                    }
                    else
                        isRingSpecial = false;

                    break;
                case MessageType.WhiteWins:
                    PrisonersTextBlock.Foreground = Resources["BlackBrush"] as SolidColorBrush;
                    TimerTextBlock.Foreground = BuyomiTextBlock.Foreground = Resources["TreeBrush"] as SolidColorBrush;

                    if (IsWhite)
                    {
                        RingMove.Stroke = Resources["BlackBrush"] as SolidColorBrush;
                        RingMove.Width = RingMove.Height = ringSize();
                        RingMove.StrokeThickness = 4;
                        RingMove.Opacity = 1;
                        isRingSpecial = true;
                    }
                    else
                        isRingSpecial = false;

                    break;
                case MessageType.Error:
                case MessageType.Simple:
                    PrisonersTextBlock.Foreground = !IsWhite ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                    TimerTextBlock.Foreground = BuyomiTextBlock.Foreground = Resources["BlackBrush"] as SolidColorBrush;

                    RingMove.Stroke = Resources["OrangeBrush"] as SolidColorBrush;
                    RingMove.Width = RingMove.Height = ringSize(true);
                    RingMove.StrokeThickness = Table.IsDuos ? 6 : 8;
                    isRingSpecial = false;
                    break;
            }
        }

        #region TimerText
        public string TimerText
        {
            get => (string)GetValue(TimerTextProperty);
            set
            {
                SetValue(TimerTextProperty, value);
                TimerTextBlock.Text = value;

            }
        }
        public static readonly DependencyProperty TimerTextProperty =
            DependencyProperty.Register("TimerText", typeof(string), typeof(Timer), new PropertyMetadata(null));
        #endregion TimerText

        #region BuyomiText
        public string BuyomiText
        {
            get => (string)GetValue(BuyomiTextProperty);
            set
            {
                SetValue(BuyomiTextProperty, value);
                BuyomiTextBlock.Text = value;
            }
        }
        public static readonly DependencyProperty BuyomiTextProperty =
            DependencyProperty.Register("BuyomiText", typeof(string), typeof(Timer), new PropertyMetadata(null));
        #endregion BuyomiText

        #region PrisonersText
        public string PrisonersText
        {
            get => (string)GetValue(PrisonersTextProperty);
            set
            {
                SetValue(PrisonersTextProperty, value);
                PrisonersTextBlock.Text = value;
            }
        }
        public static readonly DependencyProperty PrisonersTextProperty =
            DependencyProperty.Register("PrisonersText", typeof(string), typeof(Timer), new PropertyMetadata(null));
        #endregion PrisonersText

        #region IsWhite
        public bool IsWhite
        {
            get => (bool)GetValue(IsWhiteProperty);
            set
            {
                SetValue(IsWhiteProperty, value);
                PrisonersColorEllipse.Fill = value ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                PrisonersTextBlock.Foreground = !value ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
            }
        }
        public static readonly DependencyProperty IsWhiteProperty =
            DependencyProperty.Register("IsWhite", typeof(bool), typeof(Timer), new PropertyMetadata(null));
        #endregion IsWhite

        #region IsPlayer1
        public bool IsPlayer1
        {
            get => (bool)GetValue(IsPlayer1Property);
            set
            {
                SetValue(IsPlayer1Property, value);
            }
        }
        public static readonly DependencyProperty IsPlayer1Property =
            DependencyProperty.Register("IsPlayer1", typeof(bool), typeof(Timer), new PropertyMetadata(null));
        #endregion IsPlayer1

        public void ShowBuyomi(string text)
        {
            if(!Table.IsDuos)
                TimerTextBlock.RenderTransform = new CompositeTransform() { TranslateY = Table.IsFullScreen ? -5 : 0 };

            BuyomiTextBlock.Text = text;
            BuyomiTextBlock.Visibility = Visibility.Visible;
        }

    }
}
