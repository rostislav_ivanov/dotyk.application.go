﻿using Common;
using Common.DotykUserControls;
using Dotyk.Application.Go;
using Dotyk.Application.Go.Model;
using Dotyk.Application.Go.Services;
using Dotyk.Application.Go.Strings;
using Dotyk.Application.Go.ViewModels;
using Dotyk.Application.Go.Views;
using Dotyk.Shell.Utils.Session;
using EngineAdapter;
using EngineAdapter.Engine;
using EngineAdapter.Services.Engine;
using KoCloud.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public enum PageName { BotOrHuman, PreSettings, Settings, Game };
    public sealed partial class InitPage
    {
        IGameEngine gameEngine;
        IRepository repository;

        GamePage gamePage;
        MenuControl menuControl;
        private Visual _mainPageVisual;
        private bool _mainPageLoaded;
        private bool _mainPageShowed;

        PageName currentPageName;
        public InitPage()
        {
            this.InitializeComponent();
            Loaded += InitPage_Loaded;
        }

        MenuButton backButton;
        MenuButton backButtonСonfirmation;

        private async void InitPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (Table.isTable)
            {
                var shell = App.Current.ShellClient;
                await shell.WaitForTestServerIfAvaliable();
                AsyncHelpers.InitializeContextSwitcher(); //ToDo: move somewhere
                
                shell.OnApplicationSlotSizeChanged += ShellClient_OnApplicationSlotSizeChanged;
                shell.Applications.OnSubMenuAction += Applications_OnSubMenuAction;
                shell.Applications.OnSubMenu += Applications_OnSubMenu;
                shell.Applications.OnMenuCustomButtonAction += Applications_OnMenuCustomButtonAction;
                shell.OnApplicationStateChanged += ShellClient_OnApplicationStateChanged;

                Table.IsLeftToRight = shell.Location.FlowDirection == Shell.Utils.FlowDirection.LeftToRight;
                Table.IsFullScreen = shell.ApplicationSlotSize == Shell.Utils.Session.ApplicationSlotSize.Fullscreen ? true : false;
                if (DotykApplication.Current.ShellClient.Locations.Count != 5)
                {
                    if (DotykApplication.Current.ShellClient.Locations.Select(Location => Location.Location.Y).Distinct().Count() == 2)
                    {
                        Table.IsBar = true;
                    }
                    else
                    {
                        Table.IsDuos = true;
                    }
                }

                shell.OnLanguageChanged += Shell_OnLanguageChanged;
            }
            
            //God.GameParameters = new GameParameters() { BoardSize = 9, IsBlack = true, IsGameWithBot = true, BotLevel = 1, TimeLimit = new TimeLimit(0, 100, 5) };
            God.GameParameters = new GameParameters();

            ShowFirstPage();

            if (!Table.IsFullScreen && Table.IsDuos)
            {
                firstPage.Visibility = Visibility.Collapsed;
                ResizeMessage.Visibility = Visibility.Visible;
            }

            repository = new SqliteRepository();
            await repository.Initialize();
            LoadGamePage();

            backButton = new MenuButton()
            {
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = Table.IsLeftToRight ? HorizontalAlignment.Left : HorizontalAlignment.Right,
                Margin = Table.IsLeftToRight ? new Thickness(50, 50, 0, 0) : new Thickness(0, 50, 50, 0),
                IsWithShadow = true,
                IconUrl = "Shared/Back.png",
                BackgroundColor = Color.FromArgb(255, 233, 168, 99),
                Width = 300,
                Height = 300
            };
            backButton.Pressed += BackButton_Pressed;

            backButtonСonfirmation = new MenuButton()
            {
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = Table.IsLeftToRight ? HorizontalAlignment.Left : HorizontalAlignment.Right,
                Margin = Table.IsLeftToRight ? new Thickness(50, 50, 0, 0) : new Thickness(0, 50, 50, 0),
                IsWithShadow = true,
                BackgroundColor = Color.FromArgb(255, 233, 168, 99),
                ForegroundColor = Colors.White,
                Width = 300,
                Height = 300
            };
            backButtonСonfirmation.Pressed += BackButtonСonfirmation_Pressed;

            if (!Table.isTable)
                LayoutRoot.Children.Add(backButton);

            UpdateLanguage();
            //Task.Run(() => StartNewGame());
        }

        private void ShellClient_OnApplicationStateChanged(ShellClient source, Shell.Utils.Session.ApplicationState applicationState)
        {
            if (App.Current.ShellClient.ApplicationState == ApplicationState.Active)
            {
                if (currentPageName == PageName.Game)
                    Pause(false);
                else
                    KoControlsStorage.SetHitTest(true, true);
            }
            else
            {
                if (currentPageName == PageName.Game)
                    Pause(true);
                else
                    KoControlsStorage.SetHitTest(false, true);
            }
        }

        private void Shell_OnLanguageChanged(ShellClient source, string newLanguage)
        {
            UpdateLanguage();
        }

        void UpdateLanguage()
        {
            switch (currentPageName)
            {
                case PageName.BotOrHuman:
                    firstPage.UpdateLocalization();
                    break;
                case PageName.PreSettings:
                    menuControl.UpdateLocalization();
                    break;
                case PageName.Settings:
                    gameSettings.UpdateLocalization();
                    break;
                case PageName.Game:
                    gamePage.UpdateLocalization();
                    break;
            }

            backButtonСonfirmation.Text = new LocalizationItem("MenuBackButton");
            ResizeMessage.Text = Localization.GetString("ResizeWindow");
        }

        private async void ShellClient_OnApplicationSlotSizeChanged(ShellClient source, Shell.Utils.Session.ApplicationSlotSize applicationSlotSize)
        {
            Table.IsFullScreen = applicationSlotSize == Shell.Utils.Session.ApplicationSlotSize.Fullscreen ? true : false;

            if (!Table.IsFullScreen && 
               (currentPageName != PageName.BotOrHuman && God.GameParameters.IsGameWithBot == false || 
               Table.IsDuos))
            {
                if(currentPageName == PageName.Game && God.GamePageViewModel != null)
                {
                    God.GamePageViewModel.PauseCommand.Execute();
                }

                switch(currentPageName)
                {
                    case PageName.BotOrHuman:
                        firstPage.Visibility = Visibility.Collapsed;
                        break;
                    case PageName.PreSettings:
                        menuControl.Visibility = Visibility.Collapsed;
                        break;
                    case PageName.Settings:
                        gameSettings.Visibility = Visibility.Collapsed;
                        break;
                    case PageName.Game:
                        gamePage.Visibility = Visibility.Collapsed;
                        break;
                }

                KoControlsStorage.SetHitTest(false);
                ResizeMessage.Visibility = Visibility.Visible;
                DefaultBackgroundAnimation.Begin();
            }
            else
            {
                switch (currentPageName)
                {
                    case PageName.BotOrHuman:
                        firstPage.Visibility = Visibility.Visible;
                        break;
                    case PageName.PreSettings:
                        menuControl.Visibility = Visibility.Visible;
                        break;
                    case PageName.Settings:
                        gameSettings.Visibility = Visibility.Visible;
                        break;
                    case PageName.Game:
                        gamePage.Visibility = Visibility.Visible;
                        break;
                }

                KoControlsStorage.SetHitTest(true);
                ResizeMessage.Visibility = Visibility.Collapsed;

                if (currentPageName == PageName.Game &&
                    God.GamePageViewModel != null)
                {
                    God.GamePageViewModel.Pause(false);
                }
            }

            if (LayoutRoot.Children.Contains(backButton))
                LayoutRoot.Children.Remove(backButton);
            if (LayoutRoot.Children.Contains(backButtonСonfirmation))
                LayoutRoot.Children.Remove(backButtonСonfirmation);

            await KoControlsStorage.SetFullScreen(Table.IsFullScreen);
        }

        MenuSingleOrFriends firstPage;
        private void ShowFirstPage()
        {
            ProgressRingContent.Visibility = Visibility.Collapsed;

            firstPage = new MenuSingleOrFriends();
            firstPage.ParameterIsSelected += FirstPage_ParameterIsSelected;
            MainPageGrid.Children.Add(firstPage);
            currentPageName = PageName.BotOrHuman;

            App.Current.ShellClient.Menu.SetContentForMenuCustomButton(
                new Dotyk.Shell.Utils.Application.ApplicationSubMenuCustomItem
                {
                    Icon = "Assets\\Shared\\icon 512px back_color_#E9A863.png"
                });
        }

        private void FirstPage_ParameterIsSelected(object sender, int e)
        {
            God.GameParameters.IsGameWithBot = e == 0;
            ClearPage();

            ShowPreSettings();
        }

        private void ShowPreSettings()
        {
            menuControl = new MenuControl(1, 3, new LocalizationItem[3] { new LocalizationItem("PlayDefaultSettings",  9 ),
                                                                          new LocalizationItem("PlayDefaultSettings", 19 ),
                                                                          new LocalizationItem("Settings") },
                                                                          new string[3] { "9x9.png", "19x19.png", "Settings.png" });
            menuControl.ParameterIsSelected += PreSettingsParameterIsSelected;
            MainPageGrid.Children.Add(menuControl);
            currentPageName = PageName.PreSettings;
        }

        GameSettings gameSettings;
        private void PreSettingsParameterIsSelected(object sender, int e)
        {
            ClearPage();

            if (e == 2)
            {
                gameSettings = new GameSettings(God.GameParameters.IsGameWithBot);
                gameSettings.Start += GameSettings_Start;
                MainPageGrid.Children.Add(gameSettings);
                currentPageName = PageName.Settings;
            }
            else
            { 
                God.GameParameters.BoardSize = (byte)(e == 0 ? 9 : 19);
                God.GameParameters.BotLevel = 5;
                God.GameParameters.TimeLimit = new TimeLimit(900, 0, 0);
                God.GameParameters.IsBlack = (new Random(DateTime.Now.Millisecond)).Next() % 2 == 0;

                this.ProgressRingContent.Visibility = Visibility.Visible;
                Task.Run(() => StartNewGame());
                currentPageName = PageName.Game;
            }
        }

        private void GameSettings_Start(object sender, EventArgs e)
        {
            ClearPage();
            this.ProgressRingContent.Visibility = Visibility.Visible;
            Task.Run(() => StartNewGame());
            currentPageName = PageName.Game;
        }
        
        private void LoadGamePage()
        {
            gameEngine = new FuegoGameEngine(repository);
            God.GamePageViewModel = new GamePageViewModel(gameEngine, repository);

            gamePage = new GamePage();
            gamePage.DataContext = God.GamePageViewModel;
            gamePage.ChangeBackground += GamePage_ChangeBackground;
        }

        private void GamePage_ChangeBackground(object sender, MessageType e)
        {
            switch(e)
            {
                case MessageType.Simple:
                    //ToDo: exclude unnessesary animation
                    DefaultBackgroundAnimation.Begin();
                    break;
                case MessageType.Error:
                    RedBackgroundAnimation.Begin();
                    break;
                case MessageType.BlackWins:
                    BlackBackgroundAnimation.Begin();
                    break;
                case MessageType.WhiteWins:
                    WhiteBackgroundAnimation.Begin();
                    break;
            }
        }

        private void GameLoaded()
        {
            MainPageGrid.Children.Add(gamePage);
            this.ProgressRingContent.Visibility = Visibility.Collapsed;
            currentPageName = PageName.Game;

            App.Current.ShellClient.Menu.SetContentForMenuCustomButton(
                new Dotyk.Shell.Utils.Application.ApplicationSubMenuCustomItem
                {
                    Icon = "Assets\\Pause_backcolor#E59541.png"
                });
        }

        Guid activeGame;
        private async void StartNewGame()
        {
            try
            {
                var success = false;
                GoResponse resp = null;
                activeGame = Guid.NewGuid();

                //!AbortOperation &&
                for (int tries = 0; !success && tries < 5; tries++)
                {
                    //BusyMessage = "Starting game...";
                    //IsBusy = true;

                    // Create game from user's selections.
                    var p1 = new GoPlayer();
                    var p2 = new GoPlayer()
                    {
                        Komi = 6.5M
                    };

                    p1.Name = God.GameParameters.IsBlack ? "You" : God.GameParameters.IsGameWithBot ? "Computer" : "Opponent";          
                    p1.PlayerType = God.GameParameters.IsGameWithBot && !God.GameParameters.IsBlack ? PlayerType.Ai : PlayerType.Human;
                    p1.Level = God.GameParameters.BotLevel;
                    p1.LeftTime = God.GameParameters.TimeLimit.Copy();

                    p2.Name = !God.GameParameters.IsBlack ? "You" : God.GameParameters.IsGameWithBot ? "Computer" : "Opponent";
                    p2.PlayerType = God.GameParameters.IsGameWithBot && God.GameParameters.IsBlack ? PlayerType.Ai : PlayerType.Human;
                    p2.Level = God.GameParameters.BotLevel;
                    p2.LeftTime = God.GameParameters.TimeLimit.Copy();

                    var tmpState = new GoGame(
                        God.GameParameters.BoardSize,
                        p1, p2,
                        GoGameStatus.NotStarted,
                        GoColor.Black,
                        "",
                        "",
                        new List<GoMoveHistoryItem>(),
                        God.GameParameters.IsBlack);
                    resp = await God.GamePageViewModel.GameEngine.CreateGameAsync(tmpState);
                    //BusyMessage = null;
                    //IsBusy = false;

                    if (resp.ResultCode == GoResultCode.Success)
                    {
                        //if (God.GamePageViewModel._activeGameId != Guid.Empty)
                        //    await God.GamePageViewModel.GameEngine.DeleteGameAsync(God.GamePageViewModel._activeGameId);

                        activeGame = tmpState.Id;
                        success = true;
                    }
                }

                //if (AbortOperation)
                //    return;

                if (success)
                {
                    Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            await God.GamePageViewModel.LoadGame(activeGame);
                            GameLoaded();
                        });
                }
                /*else
                {
                    if (resp != null)
                        await DisplayErrorCode(resp.ResultCode);
                    else
                        await DisplayErrorCode(GoResultCode.InternalError);
                }*/
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                //BusyMessage = null;
                //IsBusy = false;
            }
        }

        private void Applications_OnMenuCustomButtonAction(ShellClient source, string action, bool isPressed)
        {
            if (currentPageName != PageName.BotOrHuman)
            {
                if(isPressed)
                {
                    if (currentPageName == PageName.Game)
                    {
                        Pause(true);
                    }

                    if (!LayoutRoot.Children.Contains(backButton))
                        LayoutRoot.Children.Add(backButton);
                }
                else
                {
                    if (currentPageName == PageName.Game)
                    {
                        Pause(false);
                    }

                    if (LayoutRoot.Children.Contains(backButton))
                        LayoutRoot.Children.Remove(backButton);
                    if (LayoutRoot.Children.Contains(backButtonСonfirmation))
                        LayoutRoot.Children.Remove(backButtonСonfirmation);
                }
            }
        }   

        private void Applications_OnSubMenu(ShellClient source, bool isPressed)
        {
            if (currentPageName == PageName.Game)
            {
                Pause(isPressed);
            }

            /*
            if (!Table.IsFullScreen)
            {
                BackButton.Visibility = Visibility.Visible;
                BackButton.IsEnabled = true;
            }
            */
        }

        private void Applications_OnSubMenuAction(ShellClient source, string action, bool isPressed)
        {
            if (String.Equals(action, "Back"))
            {
                Back();
            }
        }

        private void BackButton_Pressed(object sender, EventArgs e)
        {
            LayoutRoot.Children.Remove(backButton);
            LayoutRoot.Children.Add(backButtonСonfirmation);
        }

        private void BackButtonСonfirmation_Pressed(object sender, EventArgs e)
        {
            Back();
            LayoutRoot.Children.Remove(backButtonСonfirmation);
        }

        void Pause(bool? pause = null)
        {
            if (!God.GamePageViewModel.IsLoaded)
                return;

            if (pause == null)
                God.GamePageViewModel.PauseCommand.Execute();
            else
                God.GamePageViewModel.Pause((bool)pause);

            App.Current.ShellClient.Menu.SetContentForMenuCustomButton(
                    new Dotyk.Shell.Utils.Application.ApplicationSubMenuCustomItem
                    {
                        Icon = God.GamePageViewModel.IsPaused ? "Assets\\Play_backcolor#E59541.png" : "Assets\\Pause_backcolor#E59541.png"
                    });
        }

        void Back()
        {
            DefaultBackgroundAnimation.Begin();
            ResizeMessage.Visibility = Visibility.Collapsed;

            switch (currentPageName)
            {
                case PageName.PreSettings:
                    ClearPage();
                    ShowFirstPage();
                    break;

                case PageName.Settings:
                    ClearPage();
                    ShowPreSettings();
                    break;

                case PageName.Game:
                    ClearPage();
                    LoadGamePage();
                    ShowFirstPage();
                    break;
            }
        }

        private void ClearPage()
        {
            if (MainPageGrid.Children.Contains(menuControl))
                MainPageGrid.Children.Remove(menuControl);
            if (MainPageGrid.Children.Contains(firstPage))
                MainPageGrid.Children.Remove(firstPage);
            if (MainPageGrid.Children.Contains(gameSettings))
                MainPageGrid.Children.Remove(gameSettings);
            if (MainPageGrid.Children.Contains(gamePage))
                MainPageGrid.Children.Remove(gamePage);
        }

        /*
        private async void LoadMainPage()
        {
            if (_mainPageLoaded) return;
            _mainPageLoaded = true;

            _mainPage = new MainPage
            {
                Opacity = 1,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch
            };

            _mainPageVisual = _mainPage.GetElementVisual();
            _mainPageVisual.Opacity = 0f;
            // _mainPage.ReadyToShow += MainPage_ReadyToShow;
            ShowMainPage();
            MainPageGrid.Children.Add(_mainPage);

            //Timeout to load and show MainPage is 60 seconds
            await Task.Delay(TimeSpan.FromSeconds(60));

            if (!_mainPageShowed)
                ShowMainPage();
        }
        

        private void MainPage_ReadyToShow(object sender, EventArgs e)
        {
           // _mainPage.ReadyToShow -= MainPage_ReadyToShow;
            ShowMainPage();
        }

        private void ShowMainPage()
        {
            if (_mainPageShowed) return;
            _mainPageShowed = true;

            _mainPageVisual.AddImplicitKeyFrameAnimation(KeyFrameAnimationType.Opacity, TimeSpan.FromMilliseconds(150));
            _mainPageVisual.Compositor.CreateAnimationScopedBatch(StartAnimation, AnimationCompleted);

            void StartAnimation()
            {
                _mainPageVisual.Opacity = 1f;
            }

            void AnimationCompleted()
            {
                _mainPageVisual.ResetImplicitAnimations();
                ProgressRingContent.IsActive = false;
            }
        }
        */
    }
}
