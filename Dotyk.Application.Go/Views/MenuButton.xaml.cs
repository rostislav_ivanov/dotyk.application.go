﻿using Common.DotykUserControls;
using Dotyk.Application.Go.Strings;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class MenuButton : UserControl
    {
        Storyboard storyboardTurnAround;
        TextBlock textblock2;
        public MenuButton()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
            this.Loaded += MenuButton_Loaded;
            this.SizeChanged += MenuButton_SizeChanged;
        }

        private void MenuButton_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double diameter = Math.Min(ActualHeight, ActualWidth);
            if (diameter > 0 && !Double.IsNaN(diameter))
            {
                this.Button.Width = diameter * (1.0 - 2 * MarginPercantage);
                this.Button.Height = diameter * (1.0 - 2 * MarginPercantage);

                if (!isFontSizeSet)
                {
                    if (ButtonText.Text.Length > 30)
                        ButtonText.FontSize = Button.Height * 0.105;
                    else
                        ButtonText.FontSize = Button.Height * 0.133;

                    if(ButtonImage.Visibility == Visibility.Visible)
                        ButtonText.FontSize = ButtonText.FontSize * 0.65;
                }

                this.Button.Margin = new Thickness(diameter * MarginPercantage);

                if (ButtonText.Visibility == Visibility.Visible)
                {
                    ButtonText.Width = Button.Width * 0.8;

                    if (ButtonImage.Visibility == Visibility.Visible)
                    {
                        ButtonText.Margin = new Thickness(0, Button.Width * 0.075, 0, 0);
                        ButtonImage.Height = Button.Height * 0.4;
                    }
                }
            }

            this.Button.UpdatePosition();
        }

        public bool IsLongPopOut;

        private bool isLoaded;
        private void MenuButton_Loaded(object sender, RoutedEventArgs e)
        {
            isLoaded = true;
            if (IsFlipped)
            {
                Storyboard storyboard = this.Resources["PopOutFlipped"] as Storyboard;
                storyboard.Begin();
            }
            else if(IsLongPopOut)
            {
                Storyboard storyboard = this.Resources["LongPopOut"] as Storyboard;
                storyboard.Begin();
            }
            else
            {
                Storyboard storyboard = this.Resources["PopOut"] as Storyboard;
                storyboard.Begin();
            }

            storyboardTurnAround = this.Resources["TurnAround"] as Storyboard;
            if (isNeedToTurnAround)
            {
                storyboardTurnAround.Begin();
            }
        }

        #region IsWhite
        public bool IsWhite
        {
            get => (bool)GetValue(IsWhiteProperty);
            set
            {
                SetValue(IsWhiteProperty, value);
                if (value)
                {
                    ButtonText.Foreground = new SolidColorBrush(Colors.Black);
                    ButtonEllipse.Fill = new SolidColorBrush(Colors.White);
                    //ButtonEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 219, 219, 219)); //Resources["WhiteBrush"] as LinearGradientBrush;
                }
                else
                {
                    ButtonText.Foreground = new SolidColorBrush(Colors.White);
                    ButtonEllipse.Fill = new SolidColorBrush(Colors.Black);
                    //ButtonEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 69, 69, 69));//Resources["BlackBrush"] as LinearGradientBrush;
                }
            }
        }
        public static readonly DependencyProperty IsWhiteProperty =
            DependencyProperty.Register("IsWhite", typeof(bool), typeof(MenuButton), new PropertyMetadata(null));

        #endregion IsWhite

        #region TextKey
        /*
        public string TextKey
        {
            get => (string)GetValue(TextKeyProperty);
            set
            {
                SetValue(TextKeyProperty, value);
                ButtonText.Visibility = Visibility.Visible;
                ButtonText.Text = Localization.GetString(value);
            }
        }

        public static readonly DependencyProperty TextKeyProperty =
            DependencyProperty.Register("TextKey", typeof(string), typeof(MenuButton), new PropertyMetadata(null));

        public int[] TextParams;
        */

        ToStringClass text;
        public ToStringClass Text
        {
            set
            {
                text = value;
                ButtonText.Visibility = Visibility.Visible;
                ButtonText.Text = text.ToString();
            }
            get
            {
                return text;
            }
        }

        public void UpdateLocalization()
        {
             ButtonText.Text = text.ToString();
        }
        #endregion TextKey

        #region IconUrl
        public string IconUrl
        {
            get => (string)GetValue(IconUrlProperty);
            set
            {
                SetValue(IconUrlProperty, value);
                ButtonImage.Visibility = Visibility.Visible;
                ButtonImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/" + value));
            }
        }

        public static readonly DependencyProperty IconUrlProperty =
            DependencyProperty.Register("IconUrl", typeof(string), typeof(MenuButton), new PropertyMetadata(null));
        #endregion IconUrl

        #region IsWithShadow
        public bool IsWithShadow
        {
            get => (bool)GetValue(IsWithShadowProperty);
            set
            {
                SetValue(IsWithShadowProperty, value);
                Shadow.Visibility = Visibility.Visible;
                ButtonEllipse.RenderTransform = new ScaleTransform() { ScaleX = 0.9, ScaleY = 0.9 };
                ButtonImage.RenderTransform = new ScaleTransform() { ScaleX = 0.9, ScaleY = 0.9 };
            }
        }

        public static readonly DependencyProperty IsWithShadowProperty =
            DependencyProperty.Register("IsWithShadow", typeof(bool), typeof(MenuButton), new PropertyMetadata(null));
        #endregion IsWithShadow

        #region MarginPercantage
        public double MarginPercantage
        {
            get => (double)GetValue(MarginPercantageProperty);
            set
            {
                SetValue(MarginPercantageProperty, value);
            }
        }

        public static readonly DependencyProperty MarginPercantageProperty =
            DependencyProperty.Register("MarginPercantage", typeof(double), typeof(MenuButton), new PropertyMetadata(0.2));

        #endregion MarginPercantage

        #region Pressed
        public event EventHandler Pressed;

        private void Button_Pressed(object sender, EventArgs e)
        {
            if (IsSelected != null)
            {
                IsSelected = !IsSelected;
            }
            else
            {
                if (VerticalLine != null) VerticalLine.Stroke = new SolidColorBrush(Colors.White);
                if (HorizontalLine != null) HorizontalLine.Stroke = new SolidColorBrush(Colors.White);
            }

            Pressed?.Invoke(this, e);
        }
        #endregion

        public int Value;
        public Line VerticalLine;
        public Line HorizontalLine;

        #region Command
        public static DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(DelegateCommand), typeof(MenuButton), new PropertyMetadata(null));
        public DelegateCommand Command
        {
            get
            {
                return (DelegateCommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
                //this.Button.Command = Command;
            }
        }
        #endregion

        #region BackgroundColor
        public Color BackgroundColor
        {
            get => (Color)GetValue(BackgroundColorProperty);
            set
            {
                SetValue(BackgroundColorProperty, value);
                ButtonEllipse.Fill = new SolidColorBrush(BackgroundColor);
            }
        }

        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof(Color), typeof(MenuButton), new PropertyMetadata(null));

        #endregion BackgroundColor

        #region ForegroundColor
        public Color ForegroundColor
        {
            get => (Color)GetValue(ForegroundColorProperty);
            set
            {
                SetValue(ForegroundColorProperty, value);
                ButtonText.Foreground = new SolidColorBrush(ForegroundColor);
            }
        }

        public static readonly DependencyProperty ForegroundColorProperty =
            DependencyProperty.Register("ForegroundColor", typeof(Color), typeof(MenuButton), new PropertyMetadata(null));

        #endregion ForegroundColor

        public ButtonControl ButtonControl
        {
            get
            {
                return Button;
            }
        }

        private bool isNeedToTurnAround;
        public bool IsNeetToTurnAround
        {
            set
            {
                isNeedToTurnAround = value;
                if (storyboardTurnAround != null)
                {
                    if (isNeedToTurnAround)
                        storyboardTurnAround.Begin();
                    else
                    {
                        storyboardTurnAround.Stop();
                        CompositeTransform.Rotation = 0;
                    }
                }
            }
        }
        public bool IsFlipped;

        bool isFontSizeSet;

        #region ButtonFontSize
        public double ButtonFontSize
        {
            get => (double)GetValue(ButtonFontSizeProperty);
            set
            {
                SetValue(ButtonFontSizeProperty, value);
                ButtonText.FontSize = value;
                isFontSizeSet = true;
            }
        }

        public static readonly DependencyProperty ButtonFontSizeProperty =
            DependencyProperty.Register("ButtonFontSize", typeof(double), typeof(MenuButton), new PropertyMetadata(null));

        #endregion ButtonFontSize


        public static DependencyProperty IsSelectedProperty = DependencyProperty.Register("IsSelected", typeof(bool?), typeof(MenuButton), new PropertyMetadata(null));
        public bool? IsSelected
        {
            get
            {
                return (bool?)GetValue(IsSelectedProperty);
            }
            set
            {
                SetValue(IsSelectedProperty, value);

                if (isLoaded)
                {
                    if (value == true)
                    {
                        SelectAnimation.Begin();
                    }
                    else if (value == false)
                    {
                        UnSelectAnimation.Begin();
                    }
                }
                else
                {
                    if (value == true)
                    {
                        ButtonEllipse.Fill = Resources["TreeBrush"] as SolidColorBrush;
                        ButtonText.Foreground = new SolidColorBrush(Colors.Black);
                    }
                    else if (value == false)
                    {
                        ButtonEllipse.Fill = new SolidColorBrush(Colors.Black);
                        ButtonText.Foreground = new SolidColorBrush(Colors.White);
                    }
                }
            }
        }

        public void SetEnable(bool isEnable, bool isNeedToHide = false)
        {
            IsEnabled = isEnable;
            Button.IsEnabled = isEnable;

            if (isEnable && Command.CanExecute())
                Enable.Begin();
            else if(isNeedToHide)
                Hide.Begin();
            else
                Disable.Begin();
        }

        public void UpdatePosition()
        {
            this.Button.UpdatePosition();
        }

        public static DependencyProperty UseTouchAnimationProperty = DependencyProperty.Register("UseTouchAnimation", typeof(bool), typeof(ButtonControl), new PropertyMetadata(true));
        public bool UseTouchAnimation
        {
            get
            {
                return (bool)GetValue(UseTouchAnimationProperty);
            }
            set
            {
                SetValue(UseTouchAnimationProperty, value);
                Button.UseTouchAnimation = value;
            }
        }
        /*
        private Color RealAverageColor(bool isWhite)
        {
            return isWhite ? Color.FromArgb(255, 219, 219, 219) : Color.FromArgb(255, 69, 69, 69);
        }

        private void MenuButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ButtonEllipse.Fill = new SolidColorBrush(RealAverageColor(IsWhite));

            Storyboard storyboard = new Storyboard();
            ColorAnimation colorAnimationFill = new ColorAnimation();
            colorAnimationFill.EasingFunction = new SineEase() { EasingMode = EasingMode.EaseIn };
            colorAnimationFill.To = IsEnabled ? RealAverageColor(IsWhite) : (IsWhite ? Color.FromArgb(255, 128, 128, 128) : Color.FromArgb(255, 128, 128, 128));
            colorAnimationFill.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            colorAnimationFill.EnableDependentAnimation = true;
            storyboard.Children.Add(colorAnimationFill);
            Storyboard.SetTargetProperty(colorAnimationFill, "(Ellipse.Fill).(SolidColorBrush.Color)");
            Storyboard.SetTarget(colorAnimationFill, ButtonEllipse);

            ColorAnimation colorAnimationForeground = new ColorAnimation();
            colorAnimationForeground.EasingFunction = new SineEase() { EasingMode = EasingMode.EaseIn };
            colorAnimationForeground.To = IsEnabled ? RealAverageColor(!IsWhite) : (!IsWhite ? Color.FromArgb(255, 128, 128, 128) : Color.FromArgb(255, 128, 128, 128));
            colorAnimationForeground.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            colorAnimationForeground.EnableDependentAnimation = true;
            storyboard.Children.Add(colorAnimationForeground);
            Storyboard.SetTargetProperty(colorAnimationForeground, "(TextBlock.Foreground).(SolidColorBrush.Color)");
            Storyboard.SetTarget(colorAnimationForeground, ButtonText);

            storyboard.Begin();
            storyboard.Completed += Storyboard_Completed;
        }

        private void Storyboard_Completed(object sender, object e)
        {
            ButtonEllipse.Fill = IsWhite ? (Resources["WhiteBrush"] as SolidColorBrush) : (Resources["BlackBrush"] as SolidColorBrush);
            //ButtonEllipse.Fill = IsWhite ? new SolidColorBrush(Color.FromArgb(255, 219, 219, 219)) : new SolidColorBrush(Color.FromArgb(255, 69, 69, 69));
        }
        */
    }
}
