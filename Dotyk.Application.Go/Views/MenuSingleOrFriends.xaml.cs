﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Dotyk.Application.Go.Views
{
    public sealed partial class MenuSingleOrFriends : UserControl
    {
        public MenuSingleOrFriends()
        {
            this.InitializeComponent();
            this.SizeChanged += MenuSingleOrFriends_SizeChanged;

            UpdateLocalization();
        }

        private void MenuSingleOrFriends_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(Table.IsFullScreen || !Table.isTable)
            {
                TwoPlayersStackPanel.RenderTransform = null;
                OnePlayerButton.RenderTransform = null;

                OnePlayerButton.FontSize = 67;
                TwoPlayersButton.FontSize = 67;

                TwoPlayersStackPanel.Visibility = Visibility.Collapsed;
                TwoPlayersButton.ButtonControl.IsEnabled = true;
                TwoPlayersButton.Visibility = Visibility.Visible;

                OnePlayerButton.RenderTransform = new TranslateTransform() { Y = 0 };

                if(Table.IsDuos)
                {
                    OnePlayerButton.MarginPercantage = 0.1;
                    TwoPlayersButton.MarginPercantage = 0.1;
                }
                else
                {
                    OnePlayerButton.MarginPercantage = 0.2;
                    TwoPlayersButton.MarginPercantage = 0.2;
                }
            }
            else
            {
                TwoPlayersIcon.Width = TwoPlayersButton.ActualWidth * 0.85 * (1.0 - 2 * TwoPlayersButton.MarginPercantage);

                TwoPlayersStackPanel.RenderTransform = new TranslateTransform() { Y = 33 };
                OnePlayerButton.RenderTransform = new TranslateTransform() { Y = -33 };

                OnePlayerButton.FontSize = 52;
                TwoPlayersButton.FontSize = 52;

                TwoPlayersStackPanel.Visibility = Visibility.Visible;
                TwoPlayersButton.Visibility = Visibility.Collapsed;
                TwoPlayersButton.ButtonControl.IsEnabled = false;
            }
        }

        public event EventHandler<int> ParameterIsSelected;

        private void MenuButton_Pressed(object sender, EventArgs e)
        {
            ParameterIsSelected?.Invoke(null, 0);
        }

        private void MenuButton_Pressed2(object sender, EventArgs e)
        {
            ParameterIsSelected?.Invoke(null, 1);
        }

        public void UpdateLocalization()
        {
            OnePlayerButton.Text = new Strings.LocalizationItem("MenuSingleMode");
            TwoPlayersTextBlock.Text = Localization.GetString("MenuPlayWithFriendsResize");
            TwoPlayersButton.Text = new Strings.LocalizationItem("MenuPlayWithFriends");
        }
    }
}
