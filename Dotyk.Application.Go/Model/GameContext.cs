﻿using EngineAdapter.Engine;
using Microsoft.EntityFrameworkCore;

namespace Dotyk.Application.Go.Model
{
    public class GameContext : DbContext
    {
        public DbSet<GoGame> GoGame { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=temp3.db");
        }
    }
}