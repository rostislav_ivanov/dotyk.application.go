﻿using EngineAdapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Go.Model
{
    public class GameParameters
    {
        public int BotLevel;
        public bool IsGameWithBot;
        public byte BoardSize;
        public TimeLimit TimeLimit;
        public bool IsBlack;
    }
}
