﻿namespace Dotyk.Application.Go
{
    public enum GoMoveKind
    {
        Move, Pass, Resign
    }
}
