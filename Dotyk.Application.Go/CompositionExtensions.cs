﻿using System;
using System.Numerics;
using System.Threading.Tasks;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Hosting;

namespace Dotyk.Application.Go
{
    internal enum KeyFrameAnimationType
    {
        Opacity,
        Offset,
        Size,
        Scale,
        Orientation,
        CenterPoint,
        RotationAngleInDegrees,
        RotationAngle,
        RotationAxis,
        AnchorPoint
    }

    internal static class CompositionExtensions
    {
        #region SetValueWithoutImplicitAnimations

        public static Visual SetOpacityWithoutImplicitAnimations(this Visual visual, float opacity)
        {
            if (visual.Opacity == opacity) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.Opacity = opacity;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.Opacity = opacity;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetSizeWithoutImplicitAnimations(this Visual visual, Vector2 size)
        {
            if (visual.Size == size) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.Size = size;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.Size = size;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetScaleWithoutImplicitAnimations(this Visual visual, Vector3 scale)
        {
            if (visual.Scale == scale) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.Scale = scale;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.Scale = scale;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetOffsetWithoutImplicitAnimations(this Visual visual, Vector3 offset)
        {
            if (visual.Offset == offset) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.Offset = offset;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.Offset = offset;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetOrientationWithoutImplicitAnimations(this Visual visual, Quaternion orientation)
        {
            if (visual.Orientation == orientation) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.Orientation = orientation;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.Orientation = orientation;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetCenterPointWithoutImplicitAnimations(this Visual visual, Vector3 centerPoint)
        {
            if (visual.CenterPoint == centerPoint) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.CenterPoint = centerPoint;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.CenterPoint = centerPoint;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetRotationAngleInDegreesWithoutImplicitAnimations(this Visual visual, float angleInDegrees)
        {
            if (visual.RotationAngleInDegrees == angleInDegrees) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.RotationAngleInDegrees = angleInDegrees;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.RotationAngleInDegrees = angleInDegrees;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetRotationAngleWithoutImplicitAnimations(this Visual visual, float angleInRadians)
        {
            if (visual.RotationAngle == angleInRadians) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.RotationAngle = angleInRadians;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.RotationAngle = angleInRadians;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetRotationAxisWithoutImplicitAnimations(this Visual visual, Vector3 axis)
        {
            if (visual.RotationAxis == axis) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.RotationAxis = axis;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.RotationAxis = axis;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        public static Visual SetAnchorPointWithoutImplicitAnimations(this Visual visual, Vector2 anchorPoint)
        {
            if (visual.AnchorPoint == anchorPoint) return visual;

            if (visual.ImplicitAnimations == null)
            {
                visual.AnchorPoint = anchorPoint;
            }
            else
            {
                var saveImplicitAnimations = visual.ImplicitAnimations;
                visual.ImplicitAnimations = null;
                visual.AnchorPoint = anchorPoint;
                visual.ImplicitAnimations = saveImplicitAnimations;
            }

            return visual;
        }

        #endregion

        #region ImplicitAnimations

        public static bool HasImplicitAnimations(this Visual visual) => visual.ImplicitAnimations != null;

        public static Visual ResetImplicitAnimations(this Visual visual)
        {
            visual.ImplicitAnimations = null;
            return visual;
        }

        public static Visual SetImplicitAnimations(this Visual visual, ImplicitAnimationCollection implicitAnimations)
        {
            visual.ImplicitAnimations = implicitAnimations ?? throw new ArgumentNullException(nameof(implicitAnimations));
            return visual;
        }

        public static ImplicitAnimationCollection CreateImplicitKeyFrameAnimation(this Compositor compositor, KeyFrameAnimationType animationType, TimeSpan duration, CompositionEasingFunction easingFunction = null)
        {
            return compositor.CreateImplicitAnimationCollection().AddImplicitKeyFrameAnimation(animationType, duration, easingFunction, false);
        }

        public static ImplicitAnimationCollection AddImplicitKeyFrameAnimation(this ImplicitAnimationCollection implicitAnimations, KeyFrameAnimationType animationType, TimeSpan duration, CompositionEasingFunction easingFunction = null, bool overrideIfExist = false)
        {
            var compositor = implicitAnimations.Compositor;
            KeyFrameAnimation keyFrameAnimation = null;

            switch (animationType)
            {
                case KeyFrameAnimationType.Opacity:
                    keyFrameAnimation = compositor.CreateScalarKeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.Opacity);
                    break;
                case KeyFrameAnimationType.Offset:
                    keyFrameAnimation = compositor.CreateVector3KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.Offset);
                    break;
                case KeyFrameAnimationType.Size:
                    keyFrameAnimation = compositor.CreateVector2KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.Size);
                    break;
                case KeyFrameAnimationType.Scale:
                    keyFrameAnimation = compositor.CreateVector3KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.Scale);
                    break;
                case KeyFrameAnimationType.Orientation:
                    keyFrameAnimation = compositor.CreateQuaternionKeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.Orientation);
                    break;
                case KeyFrameAnimationType.CenterPoint:
                    keyFrameAnimation = compositor.CreateVector3KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.CenterPoint);
                    break;
                case KeyFrameAnimationType.RotationAngle:
                    keyFrameAnimation = compositor.CreateScalarKeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.RotationAngle);
                    break;
                case KeyFrameAnimationType.RotationAngleInDegrees:
                    keyFrameAnimation = compositor.CreateScalarKeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.RotationAngleInDegrees);
                    break;
                case KeyFrameAnimationType.RotationAxis:
                    keyFrameAnimation = compositor.CreateVector3KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.RotationAxis);
                    break;
                case KeyFrameAnimationType.AnchorPoint:
                    keyFrameAnimation = compositor.CreateVector2KeyFrameAnimation();
                    keyFrameAnimation.Target = nameof(Visual.AnchorPoint);
                    break;
            }

            if (keyFrameAnimation == null)
                throw new ArgumentOutOfRangeException(nameof(animationType), animationType, "Cannot create keyframe animation for the specified animation type.");

            if (easingFunction != null)
                keyFrameAnimation.InsertExpressionKeyFrame(1.0f, "this.FinalValue", easingFunction);
            else
                keyFrameAnimation.InsertExpressionKeyFrame(1.0f, "this.FinalValue");

            keyFrameAnimation.Duration = duration;

            if (implicitAnimations.ContainsKey(keyFrameAnimation.Target) && !overrideIfExist)
                throw new InvalidOperationException("A specified visual already contains a specific animation.");

            implicitAnimations[keyFrameAnimation.Target] = keyFrameAnimation;

            return implicitAnimations;
        }

        public static Visual AddImplicitKeyFrameAnimation(this Visual visual, KeyFrameAnimationType animationType, TimeSpan duration, CompositionEasingFunction easingFunction = null, bool overrideIfExist = false)
        {
            if (visual.ImplicitAnimations == null)
                visual.ImplicitAnimations = visual.Compositor.CreateImplicitAnimationCollection();

            visual.ImplicitAnimations.AddImplicitKeyFrameAnimation(animationType, duration, easingFunction, overrideIfExist);
            return visual;
        }

        public static Visual GetElementVisual(this UIElement element) => ElementCompositionPreview.GetElementVisual(element);

        public static void SetElementChildVisual(this UIElement element, Visual visual) => ElementCompositionPreview.SetElementChildVisual(element, visual);

        #endregion

        #region AnimationScopedBatch

        public static CompositionScopedBatch CreateAnimationScopedBatch(this Compositor compositor, Action startAnimation, Action animationCompleted, bool autoClose = true)
        {
            var scopedBatch = compositor.CreateScopedBatch(CompositionBatchTypes.Animation);
            scopedBatch.Completed += (s, e) => { animationCompleted(); scopedBatch.Dispose(); };
            startAnimation();

            if (autoClose)
                scopedBatch.End();

            return scopedBatch;
        }
        
        public static Task CreateAnimationScopedBatchAsync(this Compositor compositor, Action startAnimation, Action animationCompleted)
        {
            var tcs = new TaskCompletionSource<bool>();
            var scopedBatch = compositor.CreateScopedBatch(CompositionBatchTypes.Animation);
            scopedBatch.Completed += (s, e) => AnimationCompletedLocal();
            startAnimation();
            scopedBatch.End();

            void AnimationCompletedLocal()
            {
                try
                {
                    animationCompleted();
                    tcs.SetResult(true);
                }
                catch (Exception exc)
                {
                    tcs.SetException(exc);
                }
            }

            return tcs.Task;
        }
        
        #endregion
    }
}
