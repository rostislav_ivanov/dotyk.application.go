﻿using Common;
using EngineAdapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Go.Strings
{
    public class TimeLocalizationItem : ToStringClass
    {
        public TimeLimit TimeLimit;
        public bool IsButtonOrTimer;

        public TimeLocalizationItem(TimeLimit timeLimit, bool isButtonOrTimer = true)
        {
            this.TimeLimit = timeLimit;
            IsButtonOrTimer = isButtonOrTimer;
        }

        public override string ToString()
        {
            if (IsButtonOrTimer)
            {
                if (TimeLimit.InitialStones == 0)
                {
                    return String.Concat(
                                         MinOrSec(TimeLimit.MainTime));
                }
                else if (TimeLimit.InitialStones == 1 && TimeLimit.MainTime == 0)
                {
                    return String.Concat(
                                         MinOrSec(TimeLimit.InitialBuyomi),
                                         "\r\n",
                                         Localization.GetString("PerTurn"));
                }
                else if (TimeLimit.InitialStones > 1 && TimeLimit.MainTime == 0)
                {
                    return String.Concat("+ ",
                                        MinOrSec(TimeLimit.InitialBuyomi),
                                        "\r\n",
                                        String.Format(Localization.GetString("ForStones"), TimeLimit.InitialStones));
                }
                else
                {
                    return String.Concat(
                                        MinOrSec(TimeLimit.MainTime),
                                        "\r\n",
                                        "+ ",
                                        MinOrSec(TimeLimit.InitialBuyomi),
                                        "\r\n",
                                        String.Format(Localization.GetString("ForStones"), TimeLimit.InitialStones));
                }
            }
            else
            {
                if (TimeLimit.InitialStones > 0)
                    if(Table.IsDuos)
                        return String.Concat(
                                       MinOrSec(TimeLimit.InitialBuyomi),
                                       "\r\n",
                                       String.Format(Localization.GetString("ForStones"), TimeLimit.InitialStones));
                    else
                        return String.Concat(
                                        MinOrSec(TimeLimit.InitialBuyomi),
                                        " ",
                                        String.Format(Localization.GetString("ForStones"), TimeLimit.InitialStones));
                else
                    return null;
            }
        }

        private string MinOrSec(int value, bool isShorted = false)
        {
            return value % 60 == 0 ? 
                String.Concat(value / 60, " ", Localization.GetString(isShorted ? "MinutesShorted" : "Minutes")) : 
                String.Concat(value     , " ", Localization.GetString(isShorted ? "SecondsShorted" : "Seconds"));
        }
    }
}

