﻿using EngineAdapter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Go.Strings
{
    public class LocalizationItem : ToStringClass
    {
        private string TextKey;
        private string[] Params;

        public LocalizationItem(string textKey)
        {
            TextKey = textKey;
            Params = null;
        }

        public LocalizationItem(string textKey, params int[] textParams)
        {
            TextKey = textKey;
            Params = textParams?.Select(x => x.ToString(CultureInfo.CurrentCulture)).ToArray();
        }

        public LocalizationItem(string textKey, params string[] textParams)
        {
            TextKey = textKey;
            Params = textParams;
        }

        public override string ToString()
        {
            return Params != null ? String.Format(Localization.GetString(TextKey), Params) : Localization.GetString(TextKey);
        }
    }
}
