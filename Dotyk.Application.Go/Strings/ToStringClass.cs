﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotyk.Application.Go.Strings
{
    public abstract class ToStringClass
    {
        public abstract override string ToString();
    }
}
