﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;

namespace Dotyk.Application.Go
{
    public static class Localization
    {
        private static ResourceMap _resourceStringMap;
        private static ResourceContext _resourceContext;
        private static string[] AvailableLocalizationForSystemMessages = { "en", "ru" };

        public static string GetString(string key)
        {
            if(Table.isTable)
            {
                try
                {
                    return DotykResourceManager.GetLocalizedText(String.Format("{0}/Text", key));
                }
                catch
                {
                    return key;
                }
            }
            else
            {
                if (_resourceContext == null || _resourceStringMap == null)
                {
                    _resourceContext = new ResourceContext();

                    string userLanguage;
                    string existedLanguage;
                    string matchedLanguage = null;

                    for (int i = 0; i < Windows.System.UserProfile.GlobalizationPreferences.Languages.Count; i++)
                    {
                        userLanguage = Windows.System.UserProfile.GlobalizationPreferences.Languages[0].Substring(0, 2);
                        for (int j = 0; j < AvailableLocalizationForSystemMessages.Length; j++)
                        {
                            existedLanguage = AvailableLocalizationForSystemMessages[j];
                            if (userLanguage == existedLanguage)
                            {
                                matchedLanguage = userLanguage;
                                break;
                            }
                        }

                        if (matchedLanguage != null) break;
                    }

                    var lang = new List<string>();
                    lang.Add(matchedLanguage == "zh" ? "zh-Hans" : matchedLanguage ?? "en");
                    _resourceContext.Languages = lang;
                    _resourceStringMap = ResourceManager.Current.MainResourceMap.GetSubtree("Resources");
                }

                var resourceItem = _resourceStringMap.GetValue(key + "/Text", _resourceContext);

                return resourceItem == null ? key : resourceItem.ValueAsString;
            }
        }
    }
}
