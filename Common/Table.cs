﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    //contains variables which are used in a whole project and should exist in one instance all time
    //ToDo: move to God class
    public static class Table
    {
        public static bool isTable = true;

        public static bool IsFullScreen = false;
        public static bool IsDuos = false;
        public static bool IsBar = false;

        public static bool IsLeftToRight = true;
    }
}
