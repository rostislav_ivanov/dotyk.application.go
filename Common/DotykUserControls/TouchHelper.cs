﻿using Dotyk.Touch.Tools;
using System.Collections.Generic;
using System.Numerics;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Cinema.Utils
{
    public static class TouchHelper
    {
        private static readonly Dictionary<object, Rectangle> Cash = new Dictionary<object, Rectangle>();

        private static int s_colorIndex;
        private static readonly Color[] s_colors =
        {
            Colors.Red, Colors.Green, Colors.Blue,
            Colors.Brown, Colors.Yellow, Colors.Orange,
            Colors.Pink, Colors.Purple, Colors.Lime,
            Colors.Indigo, Colors.BlueViolet, Colors.DarkViolet,
            Colors.ForestGreen, Colors.Peru, Colors.SpringGreen,
            Colors.Gold, Colors.Navy, Colors.Tomato
        };

        public static double GlobalOpacity { get; set; } = 0.15;

        public static bool DrawElementTouchRegion(this UIElement elementKey, Vector2 position, Vector2 size) 
            => DrawTouchRegion(elementKey, position, size, null, null);

        public static bool DrawElementTouchRegion(this UIElement elementKey) 
            => DrawTouchRegion(elementKey, elementKey.GetOnWindowPosition().ToVector2(), elementKey.RenderSize.ToVector2(), null, null);

        public static bool RemoveElementTouchRegion(this UIElement elementKey) => RemoveTouchRegion(elementKey);

        public static bool DrawTouchRegion(object key, Vector2 position, Vector2 size, Color? color = null, double? opacity = null)
        {
            if (Window.Current.Content is Grid grid)
            {
                if (Cash.TryGetValue(key, out Rectangle rect))
                {
                    rect.Width = size.X;
                    rect.Height = size.Y;
                    rect.Margin = new Thickness(position.X, position.Y, 0, 0);
                    rect.Opacity = opacity ?? GlobalOpacity;

                    if (color.HasValue)
                        rect.Fill = color.Value.ToSolidBrush();
                }
                else
                {
                    var rectangle = new Rectangle()
                    {
                        Fill = color?.ToSolidBrush() ?? s_colors[s_colorIndex % s_colors.Length].ToSolidBrush(),
                        Opacity = opacity ?? GlobalOpacity,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Width = size.X,
                        Height = size.Y,
                        Margin = new Thickness(position.X, position.Y, 0, 0)
                    };

                    s_colorIndex++;
                    grid.Children.Add(rectangle);
                    Cash.Add(key, rectangle);
                }
                return true;
            }

            return false;
        }

        public static bool DrawEllipseTouchRegion(object key, Vector2 position, Vector2 size, Color? color = null, double? opacity = null)
        {
            if (Window.Current.Content is Grid grid)
            {
                if (Cash.TryGetValue(key, out Rectangle rect))
                {
                    rect.Width = size.X;
                    rect.Height = size.Y;
                    rect.RadiusX = size.X / 2.0;
                    rect.RadiusY = size.Y / 2.0;
                    rect.Margin = new Thickness(position.X, position.Y, 0, 0);
                    rect.Opacity = opacity ?? GlobalOpacity;

                    if (color.HasValue)
                        rect.Fill = color.Value.ToSolidBrush();
                }
                else
                {
                    var rectangle = new Rectangle()
                    {
                        Fill = color?.ToSolidBrush() ?? s_colors[s_colorIndex % s_colors.Length].ToSolidBrush(),
                        Opacity = opacity ?? GlobalOpacity,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Width = size.X,
                        Height = size.Y,
                        RadiusX = size.X / 2.0,
                        RadiusY = size.Y / 2.0,
                        Margin = new Thickness(position.X, position.Y, 0, 0)
                    };

                    s_colorIndex++;
                    grid.Children.Add(rectangle);
                    Cash.Add(key, rectangle);
                }
                return true;
            }

            return false;
        }

        public static bool RemoveTouchRegion(object key)
        {
            if (Window.Current.Content is Grid grid && Cash.TryGetValue(key, out Rectangle rect))
            {
                return grid.Children.Remove(rect) && Cash.Remove(key);
            }

            return false;
        }

        public static SolidColorBrush ToSolidBrush(this Color color)
        {
            return new SolidColorBrush(color);
        }
    }
}
