﻿using System;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Dotyk.Touch.Manipulators;
using Dotyk.Touch.Tools;
using KoCloud.Utils;
using Windows.UI.Xaml.Controls;
using System.Windows.Input;
using Cinema.Utils;
using Prism.Commands;

namespace Common.DotykUserControls
{
    public partial class ButtonControl
    {
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(
            nameof(Source), typeof(UIElement), typeof(ButtonControl), new PropertyMetadata(default(UIElement), OnSourceChanged));

        private static void OnSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //if (d is ButtonControl bc)
            //    bc.Border.Child = e.NewValue as UIElement;
        }

        public static DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(DelegateCommand), typeof(ButtonControl), new PropertyMetadata(null));
        public DelegateCommand Command
        {
            get
            {
                return (DelegateCommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }

        public UIElement Source
        {
            get => (UIElement)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }
        private KoButton _koButton;
        private bool _isPressed;
        private bool _isWorked;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _touchEnabled;

        public event EventHandler Pressed;
        public event EventHandler Released;
        public event EventHandler Selected;
        public event EventHandler Chose;
        public event EventHandler Entered;
        public event EventHandler Exited;

        public int Id;

        public bool PressScaleEnabled { get; set; } = false;
        public ButtonControl()
        {
            this.InitializeComponent();

            if (Table.isTable)
            {
                Loaded += ButtonControl_Loaded;
                Unloaded += ButtonControl_Unloaded;
            }
            else
            {
                PointerPressed += ButtonControl_PointerPressed;
                PointerReleased += ButtonControl_PointerReleased;
            }
        }

        private void ButtonControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //TouchHelper.RemoveTouchRegion(_hitTestKey);

            ScaleIn.Completed -= ScaleIn_OnCompleted;
            ScaleOut.Completed -= ScaleOut_OnCompleted;
            if (_koButton == null)
                return;
            _koButton.SetHitTest(false);
            _koButton.PointerPressed -= _koButton_PointerPressed;
            _koButton.PointerReleased -= _koButton_PointerReleased;
            //_koButton.Tapped -= _koButton_Tapped;
            _koButton.PointerEntered -= _koButton_PointerExited;
            _koButton.PointerExited -= _koButton_PointerExited1;
            _koButton.KoPadding = new KoPadding(4);
            _koButton.Dispose();
            _koButton = null;

            KoControlsStorage.Remove(this);
        }

        private async void ButtonControl_Loaded(object sender, RoutedEventArgs e)
        {
            _koButton = new KoButton(false,
                Table.IsFullScreen) //ToDo: isFullScreen
            {
                ElementType = ElementType.Circle,
                EnableDoublePrecision = true,
                MinDensity = 0.15f,
                MillisecondsForTapEnabled = 15,
                MillisecondsForPress = 15
            };

            _koButton.PointerPressed += _koButton_PointerPressed;
            _koButton.PointerReleased += _koButton_PointerReleased;
            //_koButton.Tapped += _koButton_Tapped;
            _koButton.PointerEntered += _koButton_PointerExited;
            _koButton.PointerExited += _koButton_PointerExited1;

            await Dotyk.Touch.Tools.Utils.WaitForRightWindowSize(500);
            SetHitTest(true);
            UpdatePosition();

            KoControlsStorage.AddButton(this);
        }

        private async void _koButton_PointerExited1(object sender, TapEventArgs e)
        {
            await AsyncHelpers.SwitchToContext();
            Exited?.Invoke(this, default);
        }

        private async void _koButton_PointerExited(object sender, TapEventArgs e)
        {
            await AsyncHelpers.SwitchToContext();
            Entered?.Invoke(this, default);
        }

        private async void ButtonControl_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            InPress();
            Entered?.Invoke(this, default);
        }

        private void ButtonControl_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Release();
            Exited?.Invoke(this, default);
        }

        private void _koButton_Tapped(object sender, TapEventArgs e)
        {
            //await AsyncHelpers.SwitchToContext();
            if (!PressScaleEnabled || _isPressed)
                Choose();
            else
                Select();
        }

        private async void _koButton_PointerReleased(object sender, TapEventArgs e)
        {
            await AsyncHelpers.SwitchToContext();
            if (!_isWorked)
                Release();
        }

        private async void _koButton_PointerPressed(object sender, TapEventArgs e)
        {
            await AsyncHelpers.SwitchToContext();
            InPress();
        }

        private void Select()
        {
            _isPressed = true;
            if(UseTouchAnimation)
                ScaleOut.Begin();
            _koButton?.SetHitTest(false);
            _isWorked = true;
            Selected?.Invoke(this, default);
        }

        private void Choose()
        {
            _isWorked = true;
            if (UseTouchAnimation)
                VisualStateManager.GoToState(this, "ScaleInState", false);
            _koButton?.SetHitTest(false);
            Chose?.Invoke(this, default);
        }

        private async void InPress()
        {
            if (Command != null && !Command.CanExecute() || !IsEnabled)
                return;

            _isWorked = false;
            if (_isPressed && UseTouchAnimation)
                PressScaledIn?.Begin();
            else if (UseTouchAnimation)
                VisualStateManager.GoToState(this, "PressInState", false);
            Pressed?.Invoke(this, default);

            if (Command != null && Command.CanExecute())
                Command.Execute();

            _cancellationTokenSource?.Cancel();
        }

        private void Release()
        {
            if (_isPressed && UseTouchAnimation)
                ScaleOut?.Begin();
            else if(UseTouchAnimation)
                VisualStateManager.GoToState(this, "ScaleInState", false);
                

            Released?.Invoke(this, default);
        }

        public void SetHitTest(bool value)
        {
            _touchEnabled = value;
            _koButton?.SetHitTest(value);
        }

        public void UpdatePosition()
        {
            var width = Width;
            var height = Height;

            var halfSize = new Vector2((float)(width / 2), (float)(height / 2));
            var position = this.GetOnWindowPosition(halfSize.ToPoint()).ToVector2() - halfSize;

            //TouchHelper.DrawTouchRegion(_hitTestKey, position, new Vector2((float)width, (float)height));

            _koButton?.UpdatePosition(position.X, position.Y, (float)width, (float)height);
        }
        

        private readonly Guid _hitTestKey = Guid.NewGuid();

        public void GoToNormalState()
        {
            if(UseTouchAnimation)
                ScaleIn.Begin();
            _isPressed = false;
            _cancellationTokenSource?.Cancel();
        }

        private void ScaleIn_OnCompleted(object sender, object e)
        {
            _koButton?.SetHitTest(_touchEnabled);
        }

        private async void ScaleOut_OnCompleted(object sender, object e)
        {
            try
            {
                _koButton?.SetHitTest(_touchEnabled);
                _cancellationTokenSource = new CancellationTokenSource();
                await Task.Delay(5000, _cancellationTokenSource.Token);
                GoToNormalState();
            }
            catch (OperationCanceledException)
            {
                //ignored
            }
        }

        public void SetFullScreen(bool isFullScreen)
        {
            _koButton.SetWindowState(isFullScreen);
            UpdatePosition();
        }

        public static DependencyProperty UseTouchAnimationProperty = DependencyProperty.Register("UseTouchAnimation", typeof(bool), typeof(ButtonControl), new PropertyMetadata(true));
        public bool UseTouchAnimation
        {
            get
            {
                return (bool)GetValue(UseTouchAnimationProperty);
            }
            set
            {
                SetValue(UseTouchAnimationProperty, value);
            }
        }
    }
}
