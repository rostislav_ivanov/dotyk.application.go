﻿using Common.DotykUserControls;
using Dotyk.Touch.Manipulators;
using Dotyk.Touch.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class KoControlsStorage
    {
        private static Dictionary<int, ButtonControl> ButtonControlList = new Dictionary<int, ButtonControl>();
        public static KoManipulator BoardManipulator;
        public static EventHandler FullScreen;
        private static int idIndex;

        public static void AddButton(ButtonControl button)
        {
            idIndex++;
            button.Id = idIndex;
            ButtonControlList.Add(button.Id, button);
        }

        public static async Task SetFullScreen(bool isFullScreen)
        {
            SetHitTest(false, true);
            await Dotyk.Touch.Tools.Utils.WaitForRightWindowSize(500);

            foreach (var item in ButtonControlList)
            {
                item.Value.SetFullScreen(isFullScreen);
                item.Value.UpdatePosition();
            }

            if (BoardManipulator != null)
            {
                BoardManipulator.SetWindowState(isFullScreen);
                FullScreen?.Invoke(null, null);
            }

            SetHitTest(true, true);
        }

        public static void Remove(ButtonControl button)
        {
            ButtonControlList.Remove(button.Id);
        }

        private static bool IsHittableBeforePause = true;

        public static void SetHitTest(bool isHittable, bool pause = false)
        {
            if (pause && isHittable && !IsHittableBeforePause)
                return;

            foreach (var item in ButtonControlList)
            {
                item.Value.SetHitTest(isHittable);
            }

            if (BoardManipulator != null)
                BoardManipulator.SetHitTest(isHittable);

            if(!pause)
                IsHittableBeforePause = isHittable;
        }

        public static void CreateBoardManipulator()
        {
            BoardManipulator = new KoManipulator(true, Table.IsFullScreen)
            {
                //KoPadding = new KoPadding(3),
                ElementType = ElementType.Rect
            };
        }

        public static void DisposeBoardManipulator()
        {
            BoardManipulator.Dispose();
        }
    }
}
