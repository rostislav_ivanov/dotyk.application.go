﻿using System;
using System.Numerics;
using Windows.Foundation;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Hosting;
using Dotyk.Application.Camera.Helpers;
using Dotyk.Touch.Manipulators;
using Dotyk.Touch.Tools;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;
using Dotyk.Application.Camera.ClientModel;
using Dotyk.Application.Camera.ClientModel.Model;
using KoCloud.Utils;

namespace Dotyk.Application.Camera.Views
{
    public enum SubMenuState
    {
        Hid,
        Closed,
        Selected,
        Opened
    }

    public sealed partial class SubMenu
    {
        public event EventHandler Opened;
        public event EventHandler Selected;

        public int Index { get; }
        public CameraItem VideoItem { get; }

        public SubMenuState State
        {
            get => _state;
            set
            {
                switch (value)
                {
                    case SubMenuState.Hid:
                        Hide();
                        _state = value;
                        break;
                    case SubMenuState.Closed:
                        CloseVideo();
                        _state = value;
                        break;
                    case SubMenuState.Opened:
                        //if (!(VideoItem.Watermark?.IsCompletedSuccessfully ?? true))
                        //    return;
                        _state = value;
                        Opened?.Invoke(this, null);
                        break;
                    case SubMenuState.Selected:
                        _state = value;
                        _koButton.SetHitTest(false);
                        Selected?.Invoke(this, null);
                        break;
                }
            }
        }

        private readonly BitmapImage ShadowBitmap = new BitmapImage
        {
            UriSource = new Uri("ms-appx:///Assets/Icons_Shadow.png"),
            DecodePixelWidth = 512,
            DecodePixelHeight = 512
        };

        private SpriteVisual _videoSprite;
        private Visual _thisVisual;
        private Visual _contentVisual;
        private Visual _tintVisual;
        private KoButton _koButton;
        private CompositionScopedBatch _showScopedBatch;
        private CompositionScopedBatch _openScopedBatch;
        private SubMenuState _state;
        private Visual _progressRing;
        private Visual _videoVisual;

        private string VideoName { get; set; }

        //INITIALIZATION///////////////////////////////////////////////////////////////////////////////////////////////////////////

        public SubMenu(int index, CameraItem item)
        {
            Loaded += SubMenu_Loaded;
            Index = index;
            VideoItem = item;
            VideoName = item.Name;

            InitializeComponent();
        }

        private async void SubMenu_Loaded(object sender, RoutedEventArgs e)
        {
            CreateVisuals();
            CreateKoButton();

            //var taskList = new List<Task>(2);
            ////if (VideoItem.Data is SimpleVideoItemData data && !data.Data.IsCompletedSuccessfully)
            ////    taskList.Add(data.Data);

            ////if (VideoItem.Preview is SimpleVideoItemData preview && !preview.Data.IsCompletedSuccessfully)
            ////    taskList.Add(preview.Data);

            //if (taskList.Count > 0)
            //{
            //    ShowLoader();
            //    await Task.WhenAll(taskList);
            //    HideLoader();
            //}
        }

        private void ShowLoader()
        {
            _progressRing.Opacity = 1f;
            if (State == SubMenuState.Opened)
                _tintVisual.Opacity = 0.7f;
        }

        private void HideLoader()
        {
            _progressRing.Opacity = 0f;
            _tintVisual.Opacity = State == SubMenuState.Opened ? 0.3f : 0f;
        }

        private void CreateVisuals()
        {
            var opacityImplicit = Devices.Compositor.CreateScalarKeyFrameAnimation();
            opacityImplicit.Target = "Opacity";
            opacityImplicit.InsertExpressionKeyFrame(1, "This.FinalValue");
            opacityImplicit.Duration = TimeSpan.FromMilliseconds(300);

            var offsetImplicit = Devices.Compositor.CreateVector3KeyFrameAnimation();
            offsetImplicit.Target = "Offset";
            offsetImplicit.InsertExpressionKeyFrame(1, "This.FinalValue");
            offsetImplicit.Duration = TimeSpan.FromMilliseconds(600);

            var scaleImplicit = Devices.Compositor.CreateVector3KeyFrameAnimation();
            scaleImplicit.Target = "Scale";
            scaleImplicit.InsertExpressionKeyFrame(1, "This.FinalValue");
            scaleImplicit.Duration = TimeSpan.FromMilliseconds(300);

            var implicitCollection = Devices.Compositor.CreateImplicitAnimationCollection();
            implicitCollection["Opacity"] = opacityImplicit;
            implicitCollection["Offset"] = offsetImplicit;
            implicitCollection["Scale"] = scaleImplicit;

            _thisVisual = ElementCompositionPreview.GetElementVisual(this);
            _thisVisual.Scale = Vector3.Zero;
            _thisVisual.CenterPoint = new Vector3(Settings.SubMenuSize / 2);
            _thisVisual.Offset = Settings.FlipX == 1
                ? new Vector3(Settings.SubMenuOffsetX + Settings.SubMenuDistance * (Index / Settings.SubMenuRowCount), Settings.SubMenuOffsetY + Settings.SubMenuDistance * (Index % Settings.SubMenuRowCount), 0)
                : new Vector3(Settings.AppWidth - Settings.SubMenuSize - Settings.SubMenuOffsetX - Settings.SubMenuDistance * (Index / Settings.SubMenuRowCount), Settings.SubMenuOffsetY + Settings.SubMenuDistance * (Index % Settings.SubMenuRowCount), 0);
            _thisVisual.ImplicitAnimations = implicitCollection;

            _contentVisual = ElementCompositionPreview.GetElementVisual(ContentGrig);
            _contentVisual.CenterPoint = new Vector3(Settings.SubMenuSize / 2);
            _contentVisual.Scale = Vector3.One * Settings.SubMenuScale;
            _contentVisual.ImplicitAnimations = implicitCollection;

            _tintVisual = ElementCompositionPreview.GetElementVisual(Tint);
            _tintVisual.Opacity = 0;
            _tintVisual.ImplicitAnimations = implicitCollection;

            _videoSprite = Devices.Compositor.CreateSpriteVisual();
            _videoSprite.Size = new Vector2(Settings.SubMenuInnerSize);
            _videoSprite.Offset = Vector3.UnitX * Settings.SubMenuInnerSize;
            _videoSprite.ImplicitAnimations = implicitCollection;

            _videoVisual = ElementCompositionPreview.GetElementVisual(PreviewVideo);
            _videoVisual.Offset = Vector3.UnitX * Settings.SubMenuInnerSize;
            _videoVisual.ImplicitAnimations = implicitCollection;
            //ElementCompositionPreview.SetElementChildVisual(MediaBorder, _videoSprite);

            _progressRing = ElementCompositionPreview.GetElementVisual(ProgressRing);
            _progressRing.Opacity = 0;
            _progressRing.ImplicitAnimations = implicitCollection;
        }



        private void CreateKoButton()
        {
            _koButton = new KoButton(false)
            {
                ElementType = ElementType.Circle,
                EnableDoublePrecision = true
            };
            _koButton.Tapped += _koButton_Tapped;
        }

        //METHODS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Show()
        {
            _koButton.SetHitTest(true);
            _showScopedBatch?.Dispose();
            _showScopedBatch = Devices.Compositor.CreateScopedBatch(CompositionBatchTypes.Animation);
            _showScopedBatch.Completed += Show_Completed;
            _thisVisual.Scale = Vector3.One;
            _showScopedBatch.End();

            StartVideo();
        }

        private bool _isVideoLoaded = false;
        private void StartVideo()
        {
            Task.Run(async () =>
            {
                var handle = await CameraManager.Instance.GetPreviewHandle(VideoItem);
                await AsyncHelpers.SwitchToContext();
                PreviewVideo.WindowHandle = handle;
                _isVideoLoaded = true;
                if (State == SubMenuState.Selected)
                    PreviewVideo?.Resume();
                HideLoader();
            });
        }
        private void Hide()
        {
            PreviewVideo.Pause();
            _showScopedBatch?.Dispose();

            _thisVisual.Scale = Vector3.Zero;

            State = SubMenuState.Closed;
            _isVideoLoaded = false;
            _koButton.SetHitTest(false);
        }

        public void Open(CompositionBrush brush)
        {
            if(!_isVideoLoaded)
                ShowLoader();
            PreviewVideo.Resume();
            _openScopedBatch?.Dispose();

            _videoSprite.Brush = brush;
            _videoSprite.Offset = Vector3.Zero;
            _videoVisual.Offset = Vector3.Zero;
            _tintVisual.Opacity = _progressRing.Opacity > 0 ? 0.7f : 0.3f;

            _openScopedBatch = Devices.Compositor.CreateScopedBatch(CompositionBatchTypes.Animation);
            _openScopedBatch.Completed += Open_Completed;
            _contentVisual.Scale = Vector3.One;
            _openScopedBatch.End();
        }

        private void CloseVideo()
        {
            HideLoader();
            PreviewVideo.Pause();
            _openScopedBatch?.Dispose();
            _koButton.SetHitTest(true);

            _videoSprite.Offset = Vector3.UnitX * Settings.SubMenuInnerSize;
            _videoVisual.Offset = Vector3.UnitX * Settings.SubMenuInnerSize;
            _tintVisual.Opacity = 0;
            _contentVisual.Scale = Vector3.One * Settings.SubMenuScale;
        }

        //EVENTS///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void Show_Completed(object sender, CompositionBatchCompletedEventArgs args)
        {
            var buttonBounds = TransformToVisual(Window.Current.Content).TransformBounds(new Rect(0, 0, Settings.SubMenuSize, Settings.SubMenuSize));

            _koButton.SetHitTest(true);
            _koButton.UpdatePosition((float)buttonBounds.X, (float)buttonBounds.Y, (float)buttonBounds.Width, (float)buttonBounds.Height);
        }

        private void Open_Completed(object sender, CompositionBatchCompletedEventArgs args)
        {
            _koButton.SetHitTest(true);
        }

        private void _koButton_Tapped(object sender, TapEventArgs e)
        {
            State = State == SubMenuState.Selected ? SubMenuState.Opened : SubMenuState.Selected;
        }
    }
}
