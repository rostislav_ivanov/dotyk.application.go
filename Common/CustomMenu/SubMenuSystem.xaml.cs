﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Dotyk.Application.Camera.ClientModel.Model;
using Dotyk.Application.Camera.Helpers;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Dotyk.Application.Camera.Views
{
    public sealed partial class SubMenuSystem : UserControl
    {
        public event EventHandler<CameraItem> SubMenuSelected;
        public event EventHandler<CameraItem> SubMenuOpened;
        public event EventHandler Showed;
        public event EventHandler Hided;
        private readonly List<SubMenu> _subMenus = new List<SubMenu>();
        private CompositionColorBrush _previewCameraPlayerBrush;
        private bool _subMenusOpened;

        public SubMenuSystem()
        {
            this.InitializeComponent();
        }

        private void SubMenuSystem_OnLoaded(object sender, RoutedEventArgs e)
        {
            DotykApplication.Current.ShellClient.Applications.OnMenuCustomButtonAction += ShellClient_OnMenuCustomAction;
        }

        private void ShellClient_OnMenuCustomAction(ShellClient source, string action, bool isPressed)
        {
            if (action.Equals("Show") || action.Equals("Action"))
            {
                if (isPressed)
                    ShowSubMenu();
                else
                    HideSubMenu();
            }
        }

        public void Initialize(IEnumerable<CameraItem> cameraItems)
        {
            _previewCameraPlayerBrush = Devices.Compositor.CreateColorBrush(Colors.Bisque);
            var i = 0;
            foreach (var cameraManagerSource in cameraItems)
            {
                var subMenu = new SubMenu(i++, cameraManagerSource);
                subMenu.Selected += SubMenu_Selected;
                subMenu.Opened += SubMenu_Opened;

                _subMenus.Add(subMenu);
                SubMenuSpawner.Children.Add(subMenu);
            }
        }

        private void SubMenu_Opened(object sender, EventArgs e)
        {
            var senderSubMenu = sender as SubMenu;

            SubMenuOpened?.Invoke(this, senderSubMenu.VideoItem);

        }

        private void SubMenu_Selected(object sender, EventArgs e)
        {
            var senderSubMenu = sender as SubMenu;

            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                var topIndex = (uint)(SubMenuSpawner.Children.Count - 1);
                var index = (uint)SubMenuSpawner.Children.IndexOf(senderSubMenu);
                SubMenuSpawner.Children.Move(index, topIndex);
            });

            //foreach (var subMenu in _subMenus)
            Parallel.ForEach(_subMenus, (subMenu) =>
            {
                if (subMenu.Equals(sender))
                    senderSubMenu.Open(_previewCameraPlayerBrush);
                else subMenu.State = SubMenuState.Closed;
                //}
            });

            SubMenuSelected?.Invoke(this, senderSubMenu.VideoItem);

        }
        private void HideSubMenu()
        {
            _subMenusOpened = false;

            foreach (var subMenu in _subMenus)
                subMenu.State = SubMenuState.Hid;

            Hided?.Invoke(this, default);

            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
        }

        private void ShowSubMenu()
        {
            _subMenusOpened = true;

            foreach (var subMenu in _subMenus)
                subMenu.Show();
            Showed?.Invoke(this, default);
        }
    }
}
