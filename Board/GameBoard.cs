﻿using System.Collections.Generic;
using System.Diagnostics;
using System;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Xaml.Media.Animation;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using Board.Extensions;
using EngineAdapter;
using EngineAdapter.Engine;
using Common;
using Dotyk.Touch.Manipulators;
using System.Numerics;
using Dotyk.Touch.Tools;
using Cinema.Utils;

namespace Board
{
    public sealed class GameBoard : Control
    {
        private Grid _topGrid;
        private Storyboard _displayMessageStoryboard;
        private Storyboard _hideMessageStoryboard;
        private Storyboard _displayMessageStoryboardPlayer2;
        private Storyboard _hideMessageStoryboardPlayer2;
        //private Message _messageDisplay;
        //private Message _messageDisplayPlayer2;
        private Grid _gridContainer;
        // The UI elements, indexed by their position, like "A19".
        private Dictionary<string, StoneControl> _pieces;
        private Dictionary<string, Line> _lines;
        private Border _gameBorder;
        private double _circleMargin;

        public GameBoard()
        {
            DefaultStyleKey = typeof(GameBoard);
            Loaded += GameBoard_Loaded;
            SizeChanged += GameBoard_SizeChanged;
            Unloaded += GameBoard_Unloaded;
        }

        private void GameBoard_Unloaded(object sender, RoutedEventArgs e)
        {
            KoControlsStorage.BoardManipulator.Tapped -= KoManipulator_Tapped;
            KoControlsStorage.DisposeBoardManipulator();
            KoControlsStorage.FullScreen -= UpdateManipulator;
        }

        private void GameBoard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if( this.ActualWidth == 0 || Double.IsNaN(this.ActualWidth))
                return;
           
            _gridContainer.Width = (int)(this.ActualWidth / BoardEdgeSize) * BoardEdgeSize;
            _gridContainer.Height = (int)(this.ActualHeight / BoardEdgeSize) * BoardEdgeSize;

            if (_lines == null)
                return;

            double lineThickness = BoardLineThickness();
            foreach (var line in _lines)
            {
                line.Value.StrokeThickness = lineThickness;
            }

            if (_gameBorder != null)
            {
                _gameBorder.BorderThickness = new Thickness(2.2 * lineThickness);
                _gameBorder.Margin = new Thickness(-1.1 * lineThickness);
            }

            UpdateManipulator(null, null);
        }

        private double BoardLineThickness()
        {
            double lineThickness = BoardEdgeSize == 9 ? 2.2 : (BoardEdgeSize == 13 ? 1.8 : 1.1);

            return Math.Max((lineThickness)*(this.ActualWidth / 808), 1.0);
        }

        private void GameBoard_Loaded(object sender, RoutedEventArgs e)
        {
            if (BoardEdgeSize != 0)
                CreateBoard();
        }

        protected override void OnApplyTemplate()
        {
            _topGrid = GetTemplateChild("TopGrid") as Grid;
            _gameBorder = GetTemplateChild("PART_GameBorder") as Border;
            _gridContainer = GetTemplateChild("PART_GridContainer") as Grid;

            //messageDisplay = GetTemplateChild("MessageDisplay") as Message;
            //_messageDisplayPlayer2 = GetTemplateChild("MessageDisplayPlayer2") as Message;
            //_displayMessageStoryboard = _messageDisplay.Resources["DisplayMessageStoryboard"] as Storyboard;
            //_displayMessageStoryboardPlayer2 = _messageDisplayPlayer2.Resources["DisplayMessageStoryboard"] as Storyboard;
            //_hideMessageStoryboard = _messageDisplay.Resources["HideMessageStoryboard"] as Storyboard;
            //_hideMessageStoryboardPlayer2 = _messageDisplayPlayer2.Resources["HideMessageStoryboard"] as Storyboard;

            base.OnApplyTemplate();


            DoMessageTextAnimation();
        }

        private void UpdateManipulator(object sender, EventArgs e)
        {
            if (KoControlsStorage.BoardManipulator == null)
                return; 

            var width = this.ActualWidth;
            var height = this.ActualHeight;

            var halfSize = new Vector2((float)(width / 2), (float)(height / 2));
            var position = this.GetOnWindowPosition(halfSize.ToPoint()).ToVector2() - halfSize;

            KoControlsStorage.BoardManipulator.UpdatePosition(position.X, position.Y, (float)width, (float)height);

            //TouchHelper.DrawTouchRegion("l", position, new Vector2((float)width, (float)height));
        }

        private async void KoManipulator_Tapped(object sender, TapEventArgs e)
        {
            var pos = e.Position;

            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                var stoneSize = _pieces[DecodePosition(new Point(0, 0))].ActualWidth;

                int row = (int)(e.Position.Y / stoneSize);
                int col = (int)(e.Position.X / stoneSize);

                string stonePosition = _pieces[DecodePosition(new Point(col, row))].Position;

                //SelectLines(stonePosition);
                //selectedLineX = row;
                //selectedLineY = col;

                if (PressedCommand != null && PressedCommand.CanExecute(stonePosition))
                    PressedCommand.Execute(stonePosition);
            });
        }

        private static readonly Brush LineBrush = new SolidColorBrush(Colors.Black);
        private static readonly Brush TouchedLineBrush = new SolidColorBrush(Colors.White);

        public void DisplayMessageAnimation()
        {
            _displayMessageStoryboard?.Begin();

            if(IsGameWithHuman)
            {
                _displayMessageStoryboardPlayer2?.Begin();
            }
        }

        public void HideMessageAnimation()
        {
            _hideMessageStoryboard?.Begin();

            if (IsGameWithHuman)
            {
                _hideMessageStoryboardPlayer2?.Begin();
            }
        }

        /// <summary>
        /// Adds rows and columns to the grid based on the BoardEdgeSize property, 
        /// and then populates the grid with GamePiece instances.
        /// </summary>
        double lineThickness;
        public void CreateBoard()
        {
            var edgesize = BoardEdgeSize;

            if (edgesize == 0 || _gameBorder == null || _gridContainer == null)
                return;

            KoControlsStorage.CreateBoardManipulator();
            KoControlsStorage.BoardManipulator.Tapped += KoManipulator_Tapped;
            KoControlsStorage.FullScreen += UpdateManipulator;

            _pieces = new Dictionary<string, StoneControl>();
            _lines = new Dictionary<string, Line>();

            for (int i = _gridContainer.Children.Count - 1; i >= 0; i--)
            {
                var e = _gridContainer.Children[i];
                if (!(e is Grid || e is Border))
                    _gridContainer.Children.Remove(e);
            }
            //_gridContainer.Children.Clear();
            _gridContainer.ColumnDefinitions.Clear();
            _gridContainer.RowDefinitions.Clear();

            // Add left and top column and row.
            _gridContainer.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });
            _gridContainer.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Auto) });

            // Add columns and rows for pieces.
            for (int i = 0; i < edgesize * 2; i++)
            {
                _gridContainer.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                _gridContainer.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            // Add right and bottom column and row.
            _gridContainer.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });
            _gridContainer.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Auto) });

            lineThickness = BoardLineThickness();
            // Put grids in the right place.
            Grid.SetColumn(_gameBorder, 2);
            Grid.SetRow(_gameBorder, 2);
            Grid.SetColumnSpan(_gameBorder, edgesize * 2 - 2);
            Grid.SetRowSpan(_gameBorder, edgesize * 2 - 2);
            _gameBorder.BorderThickness = new Thickness(2.2 * lineThickness);
            _gameBorder.Margin = new Thickness(-1.1 * lineThickness);


            // Add row labels and lines.
            for (int row = 0; row < edgesize; row++)
            {
                // Create row labels.
                if (ShowHeaders)
                {
                    var tb = CreateTextBlock((edgesize - row).ToString());
                    tb.HorizontalAlignment = HorizontalAlignment.Right;
                    tb.VerticalAlignment = VerticalAlignment.Center;
                    tb.SetValue(Grid.RowSpanProperty, 2);
                    tb.SetValue(Grid.RowProperty, (row * 2) + 1);
                    _gridContainer.Children.Add(tb);

                    tb = CreateTextBlock((edgesize - row).ToString());
                    tb.HorizontalAlignment = HorizontalAlignment.Left;
                    tb.VerticalAlignment = VerticalAlignment.Center;
                    tb.SetValue(Grid.ColumnProperty, (edgesize * 2) + 1);
                    tb.SetValue(Grid.RowSpanProperty, 2);
                    tb.SetValue(Grid.RowProperty, (row * 2) + 1);
                    _gridContainer.Children.Add(tb);
                }

                // Create horizontal line.
                var line = new Line
                {
                    StrokeThickness = lineThickness,// - .5,
                    Stroke = LineBrush,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Center,
                    X1 = -5,
                    X2 = short.MaxValue,
                };

                line.SetValue(Grid.ColumnProperty, 2);
                line.SetValue(Grid.ColumnSpanProperty, (edgesize * 2) - 2);
                line.SetValue(Grid.RowProperty, (row * 2) + 1);
                line.SetValue(Grid.RowSpanProperty, 2);

                _lines.Add((row).ToString(), line);
                _gridContainer.Children.Add(line);
            }

            // Add column labels and lines.
            for (int col = 0; col < edgesize; col++)
            {
                // Create column labels.
                if (ShowHeaders)
                {
                    var letter = EngineHelpers.GetColumnLetter(col);
                    var tb = CreateTextBlock(letter);
                    tb.HorizontalAlignment = HorizontalAlignment.Center;
                    tb.SetValue(Grid.ColumnSpanProperty, 2);
                    tb.SetValue(Grid.ColumnProperty, (col * 2) + 1);
                    _gridContainer.Children.Add(tb);

                    tb = CreateTextBlock(letter);
                    tb.HorizontalAlignment = HorizontalAlignment.Center;
                    tb.SetValue(Grid.RowProperty, (edgesize * 2) + 1);
                    tb.SetValue(Grid.ColumnSpanProperty, 2);
                    tb.SetValue(Grid.ColumnProperty, (col * 2) + 1);
                    _gridContainer.Children.Add(tb);
                }

                // Create vertical line.
                var line = new Line
                {
                    StrokeThickness = lineThickness,
                    Stroke = LineBrush,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    Y1 = -5,
                    Y2 = short.MaxValue,
                };

                line.SetValue(Grid.RowProperty, 2);
                line.SetValue(Grid.RowSpanProperty, (edgesize * 2) - 2);
                line.SetValue(Grid.ColumnProperty, (col * 2) + 1);
                line.SetValue(Grid.ColumnSpanProperty, 2);
                
                _lines.Add(EngineHelpers.GetColumnLetter(col), line);
                _gridContainer.Children.Add(line);
            }

            // Add dots
            switch (edgesize)
            {
                case 9:
                    AddDot(2, 2);
                    AddDot(6, 2);
                    AddDot(4, 4);
                    AddDot(2, 6);
                    AddDot(6, 6);
                    break;
                case 13:
                    AddDot(3, 3);
                    AddDot(6, 3);
                    AddDot(9, 3);
                    AddDot(3, 6);
                    AddDot(6, 6);
                    AddDot(9, 6);
                    AddDot(3, 9);
                    AddDot(6, 9);
                    AddDot(9, 9);
                    break;
                case 19:
                    AddDot(3, 3);
                    AddDot(9, 3);
                    AddDot(15, 3);
                    AddDot(3, 9);
                    AddDot(9, 9);
                    AddDot(15, 9);
                    AddDot(3, 15);
                    AddDot(9, 15);
                    AddDot(15, 15);
                    break;
            }

            // Smaller boards allow more space between pieces.
            //   _circleMargin = edgesize == 9 ? 10 : (edgesize == 13 ? 5 : 3);
            StoneControl.BoardSize = edgesize;



            for (int row = 0; row < edgesize; row++)
            {
                for (int column = 0; column < edgesize; column++)
                {
                    // Create piece (every other row/column) and set its column, row, colspan, and rowspan.
                    var gamePiece = new StoneControl(EngineHelpers.DecodePosition(column, row, edgesize), null, GoColor.Black, false, false, false);
                    gamePiece.SetValue(Grid.ColumnProperty, (column * 2) + 1);
                    gamePiece.SetValue(Grid.ColumnSpanProperty, 2);
                    gamePiece.SetValue(Grid.RowProperty, (row * 2) + 1);
                    gamePiece.SetValue(Grid.RowSpanProperty, 2);


                    //gamePiece.Pressed += GamePieceOnClick;
                    gamePiece.Exited += GamePiece_Released;
                    _pieces[DecodePosition(new Point(column, row))] = gamePiece;

                    _gridContainer.Children.Add(gamePiece);
                }
            }
            
            AttemptLinkToPieces();
        }

        private void AddDot(int col, int row)
        {
            FrameworkElement dot = null;
            switch (BoardEdgeSize)
            {
                case 19:
                    dot = new Viewbox
                    {
                        Child = new Ellipse
                        {
                            Width = lineThickness * 6,
                            Height = lineThickness * 6,
                            Margin = new Thickness(25),
                            Fill = LineBrush,
                            Stroke = LineBrush,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center
                        },
                    };
                    break;
                case 13:
                    dot = new Ellipse
                    {
                        Width = lineThickness * 6,
                        Height = lineThickness * 6,
                        Fill = LineBrush,
                        Stroke = LineBrush,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };
                    break;
                case 9:
                    dot = new Ellipse
                    {
                        Width = lineThickness * 6,
                        Height = lineThickness * 6,
                        Fill = LineBrush,
                        Stroke = LineBrush,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };
                    break;
            }

            // Put in correct position.
            dot.SetValue(Grid.ColumnProperty, col * 2 + 1);
            dot.SetValue(Grid.ColumnSpanProperty, 2);
            dot.SetValue(Grid.RowProperty, row * 2 + 1);
            dot.SetValue(Grid.RowSpanProperty, 2);

            _gridContainer.Children.Add(dot);

            UpdateManipulator(null,null);
        }

        public void FixPieces()
        {
            foreach (StoneControl p in _gridContainer.Children.Where(piece => piece is StoneControl))
            {
                p.UpdateVisualStateMainProperties();
                p.UpdateVisualStateTerritory();
            }
        }

        private void GamePiece_Released(object sender, EventArgs e)
        {
            var gamePiece = sender as StoneControl;
            if (gamePiece == null)
                return;

            if (PressedCommand != null && PressedCommand.CanExecute(gamePiece.Position))
                PressedCommand.Execute(gamePiece.Position);
            /*
            int x, y;
            EngineHelpers.EncodePosition((string)gamePiece.Position, BoardEdgeSize, out x, out y);

            if (selectedLineX != x || selectedLineY != y)
            {
                SelectLines(gamePiece.Position);
                selectedLineX = x;
                selectedLineY = y;
            }
            else
            {
                SelectLines(null);
                if (PressedCommand != null && PressedCommand.CanExecute(gamePiece.Position))
                    PressedCommand.Execute(gamePiece.Position);
            }
            */
        }

        int selectedLineX = 0;
        int selectedLineY = 0;
        private void SelectLines(string position)
        {
            _lines[EngineHelpers.GetColumnLetter(selectedLineX)].Stroke = LineBrush;
            _lines[(selectedLineY).ToString()].Stroke = LineBrush;

            if (position != null)
            {
                EngineHelpers.EncodePosition((string)position, BoardEdgeSize, out selectedLineX, out selectedLineY);

                _lines[EngineHelpers.GetColumnLetter(selectedLineX)].Stroke = TouchedLineBrush;
                _lines[(selectedLineY).ToString()].Stroke = TouchedLineBrush;
            }
        }

        private void GamePieceOnClick(object sender, EventArgs routedEventArgs)
        {
            var gamePiece = sender as StoneControl;
            if (gamePiece == null)
                return;

            //SelectLines(gamePiece.Position);
        }

        private void CreatePointIndicators(Point location, double fundgeFactor = 1.0)
        {
            Shape indicator = null;
            indicator = new Ellipse() { Height = 5 * fundgeFactor, Width = 5 * fundgeFactor };
            indicator.Fill = LineBrush;

            Grid.SetColumn(indicator, (int)location.X * 2); //Set the x location
            Grid.SetColumnSpan(indicator, 2);
            indicator.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetRow(indicator, (int)location.Y * 2);   //Set the y location
            Grid.SetRowSpan(indicator, 2);
            indicator.VerticalAlignment = VerticalAlignment.Center;
            _gridContainer.Children.Add(indicator);
        }

        /// <summary>
        /// Inverts the Y axis and moves to index base A and 1 instead of 0,0.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string DecodePosition(Point p)
        {
            return EngineHelpers.DecodePosition((int)p.X, (int)p.Y, BoardEdgeSize);
        }

        private Point GetMouseHoverPoint(Pointer elementPoint, UIElement relativeTo)
        {
            return elementPoint.GetPosition(relativeTo);
        }

        private TextBlock CreateTextBlock(string content)
        {
            var tb = new TextBlock
            {
                FontWeight = FontWeights.Bold,
                FontSize = 16,
                Text = content,
                Foreground = new SolidColorBrush(Color.FromArgb(139, 234, 234, 234)),
                Margin = new Thickness(5, 1, 5, 1)
            };
            return tb;
        }

        #region Pieces
        public Dictionary<string, PieceStateViewModel> Pieces
        {
            get => (Dictionary<string, PieceStateViewModel>)GetValue(PiecesProperty); set => SetValue(PiecesProperty, value);
        }
        public static readonly DependencyProperty PiecesProperty =
            DependencyProperty.Register("Pieces", typeof(Dictionary<string, PieceStateViewModel>), typeof(GameBoard), new PropertyMetadata(null, OnPiecesChanged));
        #endregion Pieces

        private static void OnPiecesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var board = sender as GameBoard;
            Debug.Assert(board != null);

            if (e.OldValue != null)
            {
                // Unwind event subscriptions on old pieces.
                var oldPieces = e.OldValue as Dictionary<string, PieceStateViewModel>;
                Debug.Assert(oldPieces != null);
                foreach (var piece in oldPieces.Values)
                {
                    piece.MultiplePropertiesChanged -= board.piece_MultiplePropertiesChanged;
                    piece.TerritoryChanged -= board.piece_TerritoryChanged;
                }
            }

            board.AttemptLinkToPieces();
        }

        private void AttemptLinkToPieces()
        {
            // If the board has been initialized, we create the linkages.  If not, we don't.
            // This method is called when Pieces changes, and after the board is loaded, so
            // the linkage will happen in one place or another, maybe both.
            if (_pieces == null || Pieces == null)
                return;

            // Avoid double subscription.
            foreach (var piece in Pieces.Values)
            {
                piece.MultiplePropertiesChanged -= piece_MultiplePropertiesChanged;
                piece.TerritoryChanged -= piece_TerritoryChanged;
            }

            // This allows us to respond to changes in each individual piece 
            // and reflect it on the corresponding GamePiece control.
            foreach (var piece in Pieces.Values)
            {
                // Set the initial state on the control.
                piece_MultiplePropertiesChanged(piece, null);
                piece_TerritoryChanged(piece, null);
                // Respond to future updates.
                piece.MultiplePropertiesChanged += piece_MultiplePropertiesChanged;
                piece.TerritoryChanged += piece_TerritoryChanged;
            }
        }

        void piece_TerritoryChanged(object sender, EventArgs args)
        {
            var piece = (PieceStateViewModel)sender;

            // Get and update the UI control.
            var ctl = _pieces[piece.Position];
            Debug.Assert(ctl != null, "ctl was null");
            ctl.Territory = piece.Territory;

            ctl.UpdateVisualStateTerritory();
        }

        void piece_MultiplePropertiesChanged(object sender, EventArgs args)
        {
            var piece = (PieceStateViewModel)sender;

            // Get and update the UI control.
            var ctl = _pieces[piece.Position];
            Debug.Assert(ctl != null, "ctl was null");
            ctl.Sequence = piece.Sequence;
            ctl.Color = piece.Color;
            ctl.IsHint = piece.IsHint;
            ctl.IsLastMove = piece.IsNewPiece;
            ctl.IsNewCapture = piece.IsNewCapture;

            ctl.UpdateVisualStateMainProperties();
        }

        #region CurrentPointerPosition
        /// <summary>
        /// Give the Pressed command the response of GO Positions
        /// </summary>
        public string CurrentPointerPosition
        {
            get => (string)GetValue(CurrentPointerPositionProperty); set => SetValue(CurrentPointerPositionProperty, value);
        }
        public static readonly DependencyProperty CurrentPointerPositionProperty =
            DependencyProperty.Register("CurrentPointerPosition", typeof(string), typeof(GameBoard), new PropertyMetadata(null, CurrentPointerPositionChanged));

        private static void CurrentPointerPositionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {

        }
        #endregion CurrentPointerPosition

        #region PressedCommand
        /// <summary>
        /// Command for ViewModel to execute the command for pressing.
        /// </summary>
        public ICommand PressedCommand
        {
            get => (ICommand)GetValue(PressedCommandProperty); set => SetValue(PressedCommandProperty, value);
        }
        public static readonly DependencyProperty PressedCommandProperty =
            DependencyProperty.Register("PressedCommand", typeof(ICommand), typeof(GameBoard), new PropertyMetadata(null));
        #endregion PressedCommand

        #region BoardEdgeSize
        public int BoardEdgeSize
        {
            get => (int)GetValue(BoardEdgeSizeProperty); set => SetValue(BoardEdgeSizeProperty, value);
        }
        public static readonly DependencyProperty BoardEdgeSizeProperty =
            DependencyProperty.Register("BoardEdgeSize", typeof(int), typeof(GameBoard), new PropertyMetadata(0, BoardEdgeSizePropertyChangedCallback));

        private static void BoardEdgeSizePropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            //if ((int)args.NewValue != 0)
            //    ((GameBoard)obj).CreateBoard();
        }

        #endregion BoardEdgeSize

        #region ShowHeaders
        public bool ShowHeaders
        {
            get => (bool)GetValue(ShowHeadersProperty); set => SetValue(ShowHeadersProperty, value);
        }
        public static readonly DependencyProperty ShowHeadersProperty =
            DependencyProperty.Register("ShowHeaders", typeof(bool), typeof(GameBoard), new PropertyMetadata(false));
        #endregion ShowHeaders

        #region MessageText
        public string MessageText
        {
            get => (string)GetValue(MessageTextProperty);
            set => SetValue(MessageTextProperty, value);
        }
        public static readonly DependencyProperty MessageTextProperty =
            DependencyProperty.Register(nameof(MessageText), typeof(string), typeof(GameBoard), new PropertyMetadata(null, MessageTextPropertyChangedCallback));
        private static void MessageTextPropertyChangedCallback(DependencyObject dobj, DependencyPropertyChangedEventArgs args)
        {
            var gameBoard = (GameBoard) dobj;
            gameBoard.DoMessageTextAnimation();
        }

        private void DoMessageTextAnimation()
        {
            if (MessageText == null)
                HideMessageAnimation();
            else
                DisplayMessageAnimation();
        }

        #endregion MessageText

        #region IsBusy
        public bool IsBusy
        {
            get => (bool)GetValue(IsBusyProperty); set => SetValue(IsBusyProperty, value);
        }
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(GameBoard), new PropertyMetadata(false));
        #endregion IsBusy

        #region IsGameWithHuman
        public bool IsGameWithHuman
        {
            get => (bool)GetValue(IsGameWithHumanProperty);
            set => SetValue(IsGameWithHumanProperty, value);
        }
        public static readonly DependencyProperty IsGameWithHumanProperty =
            DependencyProperty.Register("IsGameWithHuman", typeof(bool), typeof(GameBoard), new PropertyMetadata(false));
        #endregion IsGameWithHuman
    }
}