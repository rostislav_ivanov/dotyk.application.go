﻿using System;
using EngineAdapter.Engine;

namespace Board.Interface
{
    public interface IGenerateMoves
    {
        event EventHandler<GoMoveResultEventArgs> GoMoveChangedEvent;
    }


    public class GoMoveResultEventArgs : EventArgs
    {
        public GoMoveResult MoveResult { get; set; }
    }
}
