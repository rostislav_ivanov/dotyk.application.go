﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Board
{
    public sealed partial class Message : UserControl
    {
        public Message()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
        }

        #region MessageText
        public string MessageText
        {
            get => (string)GetValue(MessageTextProperty);
            set
            {
                SetValue(MessageTextProperty, value);
            }
        }
        public static readonly DependencyProperty MessageTextProperty =
            DependencyProperty.Register("MessageText", typeof(string), typeof(Message), new PropertyMetadata(null));
        #endregion MessageText

        #region IsBusy
        public bool IsBusy
        {
            get => (bool)GetValue(IsBusyProperty);
            set
            {
                SetValue(IsBusyProperty, value);
            }
        }
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(Message), new PropertyMetadata(false));
        #endregion IsBusy
    }
}
