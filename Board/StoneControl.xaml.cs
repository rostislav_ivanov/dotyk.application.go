﻿using Common.DotykUserControls;
using EngineAdapter.Engine;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Board
{
    public sealed partial class StoneControl : UserControl
    {
        public static int BoardSize;
        // This constructor is included so Blend can instantiate the class.
        public StoneControl()
        {
            InitializeComponent();

            Sequence = "361";
            Color = GoColor.White;
            IsHint = true;
            IsLastMove = true;
            IsNewCapture = true;
            //CircleMargin = new Thickness(2);

            IsTabStop = false;

            Loaded += OnLoaded;
        }

        public StoneControl(string postion, string sequence, GoColor? color, bool isHint, bool isNewPiece, bool isNewCapture)
        {
            InitializeComponent();

            Position = postion;
            Sequence = sequence;
            Color = color;
            IsHint = isHint;
            IsLastMove = isNewPiece;
            IsNewCapture = isNewCapture;

            if(BoardSize > 11)
            {
                CapturedIndicatorLine1.X2 = 10;
                CapturedIndicatorLine1.Y1 = 10;
                CapturedIndicatorLine1.StrokeThickness = 1;
                CapturedIndicatorLine2.X2 = 10;
                CapturedIndicatorLine2.Y2 = 10;
                CapturedIndicatorLine2.StrokeThickness = 1;
            }

            Loaded += OnLoaded;
        }

        public string Position { get; set; }

        // These properties are from the PieceState.
        public GoColor? Color { get; set; }
        public GoColor? Territory { get; set; }
        public bool IsHint { get; set; }
        public bool IsLastMove { get; set; }
        public bool IsNewCapture { get; set; }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateVisualStateMainProperties();
            UpdateVisualStateTerritory();
        }

        internal void UpdateVisualStateTerritory()
        {
            if (Territory != null)
                VisualStateManager.GoToState(this, Territory.Value + "Territory" + Color, true);
            else
                VisualStateManager.GoToState(this, "NoTerritory", true);

        }

        internal void UpdateVisualStateMainProperties()
        {
            if (IsHint)
            {
                VisualStateManager.GoToState(this, "UndoMove" + Color, true); //ToDo: add undo field
                return;
            }

            if (Color == null)
            {
                if (IsHint)
                {
                    if (IsNewCapture)
                        VisualStateManager.GoToState(this, "HintNewCapture", true);
                    else
                        VisualStateManager.GoToState(this, "Hint", true); //ToDo: add undo field
                }
                else
                {
                    if (IsNewCapture)
                        VisualStateManager.GoToState(this, "NewCapture", true);
                    else
                        VisualStateManager.GoToState(this, "Blank", true);
                }
            }
            else
            {
                if (IsLastMove)
                    VisualStateManager.GoToState(this, Color + "LastMove", true);
                else
                    VisualStateManager.GoToState(this, Color.ToString(), true);
            }
        }

        public event EventHandler Pressed;
        public event EventHandler Released;
        public event EventHandler Exited;

        #region Sequence
        public string Sequence
        {
            get => (string)GetValue(SequenceProperty); set => SetValue(SequenceProperty, value);
        }
        public static readonly DependencyProperty SequenceProperty =
            DependencyProperty.Register("Sequence", typeof(string), typeof(StoneControl), new PropertyMetadata(null));
        #endregion Sequence

        /*#region CircleMargin
        public Thickness CircleMargin
        {
            get => (Thickness)GetValue(CircleMarginProperty);
            set
            {
                SetValue(CircleMarginProperty, value);
                //PART_Ellipse.Margin = CircleMargin;
                //PART_HintEllipse.Margin = CircleMargin;
            }
        }
        public static readonly DependencyProperty CircleMarginProperty =
            DependencyProperty.Register("CircleMargin", typeof(Thickness), typeof(StoneControl), new PropertyMetadata(5D));
        #endregion CircleMargin
        */

        private void Container_Pressed(object sender, EventArgs e)
        {
            Pressed?.Invoke(this, e);
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newSize = Math.Min(ActualHeight, ActualWidth);

            if (newSize > 0 && !Double.IsNaN(newSize))
            {
                Container.Width = newSize;
                Container.Height = newSize;

                //if (this.ActualHeight > 0 && !Double.IsNaN(ActualHeight))
                //    CircleMargin = new Thickness(this.ActualHeight * 0.1);
                //Container.UpdatePosition();
            }

            CapturedIndicatorLine1.StrokeThickness = 1 * ActualHeight / 40;
            CapturedIndicatorLine2.StrokeThickness = 1 * ActualHeight / 40;
            CapturedIndicatorLine1.X2 = 10 * ActualHeight / 40;
            CapturedIndicatorLine1.Y1 = 10 * ActualHeight / 40;
            CapturedIndicatorLine2.X2 = 10 * ActualHeight / 40;
            CapturedIndicatorLine2.Y2 = 10 * ActualHeight / 40;
            LastMoveIndicator.Margin = new Thickness(Math.Ceiling(ActualHeight / 6));
        }

        private void Container_Released(object sender, EventArgs e)
        {
            Released?.Invoke(this, e);
        }

        private void Container_Exited(object sender, EventArgs e)
        {
            Exited?.Invoke(this, e);
        }

        private void Container_Exited(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            Exited?.Invoke(this, null);
        }

        public void UpdatePosition()
        {
            //Container.UpdatePosition();
        }
    }
}
